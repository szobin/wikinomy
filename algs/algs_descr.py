#!/usr/bin/python
# -*- coding: utf-8 -*-
from algs.algs_public import algs_db_error_page
from pcode.db_lib import *
from pcode.conf import temp_path
from django.template.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
import os


def algs_descr_new(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    info = get_safe_session_param(request, 'info')
    if len(info) > 0:
        del request.session["info"]

    v = dict(code=code, user=user, info=info,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_descr_new_param_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_descr_new_upload(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/descr/')

    user = get_safe_session_param(request, 'login')

    # 1. check uploaded xml file
    try:
        xml_data = request.FILES['descr_file']
        if xml_data.size == 0:
            request.session['info'] = 'upload error: zero file length'
            return HttpResponseRedirect('/new/descr/')
    except Exception as ex:
        request.session['info'] = 'upload raise: '+str(ex)
        return HttpResponseRedirect('/new/descr/')

    xml_fn = xml_data.name

    # 2. read uploaded file
    rows = safe_check_cr_crlf(xml_data.readlines())

    xml_text = make_text_from_rows(rows)
    xml_tag = axgs_xml_parse(xml_text)
    if xml_tag.tag != "wiki":
        request.session['info'] = 'Upload error: wrong wiki descriptor format: '+xml_fn
        return HttpResponseRedirect('/new/descr/')

    # 3. write uploaded file
    fn = temp_path(user + "_" + safe_filename(xml_fn))
    f = open(fn, 'w')
    try:
        f.writelines(rows)
    finally:
        f.close()

    return HttpResponseRedirect('/new/descr/preview?fn=%s' % xml_fn)


def algs_descr_new_preview(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/descr/')

    user = get_safe_session_param(request, 'login')

    # 1. Check params
    xml_fn = request.GET.get('fn')
    if (xml_fn is None) or (len(xml_fn) == 0):
        HttpResponseRedirect('/new/descr/')

    # 2. Load descriptions
    fn = temp_path(user + "_" + safe_filename(xml_fn))
    f = open(fn, 'r')
    try:
        rows = f.readlines()
    finally:
        f.close()

    xml_text = make_text_from_rows(rows)
    xml_tag = axgs_xml_parse(xml_text)
    if xml_tag.tag != "wiki":
        request.session['info'] = 'Upload error: wrong wiki descriptor format: ' + xml_fn
        return HttpResponseRedirect('/new/descr/')

    # 3. Show list of the descriptions
    html = get_template('alg_descr_new_preview_%s.html' % alg_detect_lang(request))
    v = dict(cells=get_descr_cells(xml_tag),
             code=code, user=user,
             fn=xml_fn)
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_descr_new_insert(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/descr/')

    user = get_safe_session_param(request, 'login')

    # 1. Check params
    xml_fn = request.POST.get('fn')
    if (xml_fn is None) or (len(xml_fn) == 0):
        return HttpResponseRedirect('/new/descr/')

    # 2. Load descriptions

    fn = temp_path(user + "_" + safe_filename(xml_fn))
    f = open(fn, 'r')
    try:
        rows = f.readlines()
    finally:
        f.close()

    xml_text = make_text_from_rows(rows)
    xml_tag = axgs_xml_parse(xml_text)
    if xml_tag.tag != "wiki":
        request.session['info'] = 'Upload error: wrong wiki descriptor format: ' + xml_fn
        return HttpResponseRedirect('/new/descr/')

    # 3. Insert info
    cells = get_descr_cells(xml_tag)
    for cell in cells:
        s_type = cell[1]

        if s_type == "namespace":
            (name_id, info) = do_update_group_info(code, cell[2], cell[5])
            if name_id is None:
                return algs_db_error_page(request, info)

        if s_type == "tag":
            (name_id, info) = do_update_name_tag(code, "", cell[2], cell[2], cell[5], 0)
            if name_id is None:
                return algs_db_error_page(request, info)

        if s_type == "alg":
            field_name = cell[3]
            if field_name == "tags":
                (name_id, info) = do_update_alg_def_field(code, cell[2], "", "", "")
                if name_id is None:
                    return algs_db_error_page(request, info)

                info = do_update_name_tags(code, name_id, cell[5])
                if info is not None:
                    return algs_db_error_page(request, info)
            else:
                (name_id, info) = do_update_alg_def_field(code, cell[2], cell[4], field_name, cell[5])
                if name_id is None:
                    return algs_db_error_page(request, info)

    try:
        os.remove(fn)
    except:
        pass
    axgs_clear_cache()
    return HttpResponseRedirect('/')
