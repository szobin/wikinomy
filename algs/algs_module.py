#!/usr/bin/python
# -*- coding: utf-8 -*-
from algs.algs_public import algs_db_error_page
from pcode.db_lib import *
from django.template.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
import base64


def algs_module_new(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    alg_title = request.GET.get('title')
    if alg_title is None:
        alg_title = ""

    v = dict(alg_title=alg_title,
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_module_new_param_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_module_new_upload(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/module/')

    alg_title = request.POST.get('alg_title')

    # 1. check uploaded file alg
    try:
        alg_data = request.FILES['alg_file']
        if alg_data.size == 0:
            return HttpResponseRedirect('/new/module/?title=%s' % alg_title)
    except:
        return HttpResponseRedirect('/new/module/?title=%s' % alg_title)

    # 2. read uploaded file alg
    alg_info = alg_data.read()
    par_alg_content = base64.b64encode(alg_info)
    par_alg_fn = alg_data.name

    # 3. transfer and convert uploaded data
    report = call_axgs_wiki_data('a_module_upload',
                                 {'code': code, 'alg_code_content': par_alg_content, 'alg_code_fn': par_alg_fn})
    if len(report) == 0:
        return algs_db_error_page(request, 'conversion: internal error')

    if report.count('?xml') > 0:
        tag_xml = axgs_xml_parse(report)
        info = axgs_get_error(tag_xml)
        if info is not None:
            return algs_db_error_page(request, info)
        return algs_db_error_page(request, report)

    if report.lower().count("error") > 0:
        html = get_template('alg_conv_err_%s.html' % alg_detect_lang(request))
        v = dict(alg_title=alg_title, report=report)
        v.update(csrf(request))
        return HttpResponse(html.render(v))

    (par_alg_fn2, extension) = os.path.splitext(par_alg_fn)
    return HttpResponseRedirect('/new/module/preview?fn=%s' % par_alg_fn2)


def algs_module_new_preview(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/module/')

    user = get_safe_session_param(request, 'login')

    fn = request.GET.get('fn')
    if (fn is None) or (len(fn) == 0):
        HttpResponseRedirect('/')

    code_xml = call_axgs_wiki('a_module_preview', {'code': code, 'alg_code_fn': fn})
    info = axgs_get_error(code_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_info = get_module_info(code_xml)
    funcs = get_module_cells(code_xml)

    author = module_info['author_name']
    if len(author) > 0:
        if user != author:
            return algs_db_error_page(request, "could not load module by other author")

    rq_nss = module_info['required_nss']
    wiki_names = get_wiki_names(rq_nss)
    do_update_funcs(funcs, wiki_names)

    html = get_template('alg_module_new_preview_%s.html' % alg_detect_lang(request))
    v = dict(code=code, user=user,
             project_name=module_info['project'],
             module_name=module_info['name'],
             namespace=module_info['namespace'],
             author=user,
             nss=rq_nss,
             date=module_info['date'],
             funcs=funcs, fn=fn)
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_module_new_insert(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/module/')

    user = get_safe_session_param(request, 'login')

    # 1. Check params
    fn = request.POST.get('fn')
    if (fn is None) or (len(fn) == 0):
        HttpResponseRedirect('/')

    # 2. Fetch code info
    code_xml = call_axgs_wiki('a_module_preview', {'code': code, 'alg_code_fn': fn})
    info = axgs_get_error(code_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_info = get_module_info(code_xml)
    funcs = get_module_cells(code_xml)

    rq_nss = module_info['required_nss']
    wiki_names = get_wiki_names(rq_nss)
    do_update_funcs(funcs, wiki_names)

    namespace = module_info['namespace']
    if len(namespace) == 0:
        return algs_db_error_page(request, "namespace value is not assigned")

    module_name = module_info['name']
    if len(module_name) == 0:
        return algs_db_error_page(request, "module name is not assigned")

    author = module_info['author_name']
    if len(author) > 0:
        if user != author:
            return algs_db_error_page(request, "could not load module by other author")

    # 3. Insert group info (#1)
    (group_id, info) = do_update_group_info(code, namespace, "")
    if group_id is None:
        return algs_db_error_page(request, "namespace update error: "+info)

    # 4. Insert user info
    user_name = user
    (user_id, info) = do_update_nickname_info(code, user_name)
    if user_id is None:
        return algs_db_error_page(request, "user update error: "+info)

    # 5. Insert module info (#2)
    (module_id, info) = do_update_module_info(code, module_name, user_id)
    if module_id is None:
        return algs_db_error_page(request, "module update error: "+info)

    # 6. Project
    project_name = module_info['project']
    project_id = None
    if project_name is not None:
        (project_id, info) = do_update_project_info(code, project_name, group_id)
        if project_id is None:
            return algs_db_error_page(request, "project update error: " + info)

    # 7. Insert module algs
    info = do_update_module_funcs(funcs, code, group_id, module_id, project_id, wiki_names)
    if info is not None:
        return algs_db_error_page(request, "update funcs error: "+info)

    # 8. Move module to repository
    info = do_move_module_file_to_repository(code, fn, user_name, module_name)
    if info is not None:
        return algs_db_error_page(request, "move module to repository error: "+info)

    return HttpResponseRedirect('/module/%s.%s/' % (user_name, module_name))
