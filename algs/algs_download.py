#!/usr/bin/python
# -*- coding: utf-8 -*-
from algs.algs_public import algs_db_error_page
from pcode.db_lib import *
from django.template.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
import base64


def algs_download_module(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    # 1. init
    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    m_path = s_path[3]
    module_names = m_path.split('.')
    if len(module_names) < 2:
        return HttpResponseRedirect('/')

    module_name = module_names[len(module_names) - 1]
    owner_name = module_names[len(module_names) - 2]

    # 2. rq root info (cats & tags)
    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki_cached('q_module_download',
                                    {'module': module_name, 'owner': owner_name,
                                     'lang': alg_detect_lang(request)},
                                    120)
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_tag = tag_xml[0]
    info = module_tag.get("info")
    if info is not None:
        return algs_db_error_page(request, info)

    module_name = module_tag.get("module_name")
    group_name = module_tag.get("group_name")
    create_date = module_tag.get("create_date")

    prog_langs_tag = tag_xml[1]
    packets = get_module_packets(prog_langs_tag, module_tag)

    html = get_template('alg_module_download_%s.html' % alg_detect_lang(request))
    v = dict(module_name=module_name, group_name=group_name,
             owner_name=owner_name, create_date=create_date,
             packets=packets,
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_code_delphi(request):
    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponse(status=404, content='404 wrong rq')

    a_path = s_path[3]
    module_names = a_path.split('.')
    if len(module_names) < 2:
        return HttpResponse(status=404, content='404 wrong path: '+a_path)

    fix_list = "0"

    owner_name = module_names[0]
    module_name = module_names[1]

    project_name = owner_name+'_'+module_name
    fn = project_name+'.zip'
    modules = list()
    modules.append([owner_name+'.'+module_name, project_name+'.pas'])

    wsa_config = make_wsa_pack_config(dict(project_name=project_name,
                                           modules=modules,
                                           fix_list=fix_list,
                                           compile='delphi'))

    zip_content = call_axgs_wiki_data('q_module_zip',
                                      {'project_alias': project_name,
                                       'wsa_config': base64.b64encode(wsa_config)})
    if zip_content is None:
        return HttpResponse(status=500, content='500 wrong source rq: '+a_path)

    if zip_content.count('?xml') > 0:
        xml = axgs_xml_parse(zip_content)
        info = axgs_get_error(xml, 2)
        return HttpResponse(status=500, content='500 wrong source rq: '+a_path+' info: '+info)

    tag_xml = call_axgs_wiki('u_download_info',
                             {'module_name': module_name, 'owner_name': owner_name,
                              'prog_lang_id': "1"})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename="' + fn + '"'
    response.content = zip_content
    return response


def algs_code_cpp(request):
    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponse(status=404, content='404 wrong rq')

    a_path = s_path[3]
    module_names = a_path.split('.')
    if len(module_names) < 2:
        return HttpResponse(status=404, content='404 wrong path: '+a_path)

    fix_list = "0"

    owner_name = module_names[0]
    module_name = module_names[1]

    project_name = owner_name+'_'+module_name
    fn = project_name+'.zip'
    modules = list()
    modules.append([owner_name+'.'+module_name, project_name+'.cpp'])

    wsa_config = make_wsa_pack_config(dict(project_name=project_name,
                                           modules=modules,
                                           fix_list=fix_list,
                                           compile='cpp'))

    zip_content = call_axgs_wiki_data('q_module_zip',
                                      {'project_alias': project_name,
                                       'wsa_config': base64.b64encode(wsa_config)})
    if zip_content is None:
        return HttpResponse(status=500, content='500 wrong source rq: '+a_path)

    if zip_content.count('?xml') > 0:
        xml = axgs_xml_parse(zip_content)
        info = axgs_get_error(xml, 2)
        return HttpResponse(status=500, content='500 wrong source rq: '+a_path+' info: '+info)

    tag_xml = call_axgs_wiki('u_download_info',
                             {'module_name': module_name, 'owner_name': owner_name,
                              'prog_lang_id': "2"})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename="' + fn + '"'
    response.content = zip_content
    return response
