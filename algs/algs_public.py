#!/usr/bin/python
# -*- coding: utf-8 -*-

from pcode.db_lib import *
from wikinomy import settings
from django.template.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.template.loader import get_template
import urllib


def algs_db_error_page(request, info):
    if info.find("not found") >= 0:
        return algs_not_found(request)

    if 'code' in request.session:
        del request.session['code']

    html = get_template('alg_error_%s.html' % alg_detect_lang(request))
    if settings.DEBUG:
        v = dict(info=urllib.unquote(info))
    else:
        info = get_template('alg_error_info_%s.html' % alg_detect_lang(request))
        v = dict(info=info.render())

    return HttpResponse(html.render(v))


def algs_static(request):
    p = request.path
    need_lang = (p.count('.txt') > 0) or (p.count('.xml') > 0)
    if need_lang:
        fn = settings.STATICFILES_DIRS[0] + insert_lang_to_fn(p, alg_detect_lang(request))
    else:
        fn = settings.STATICFILES_DIRS[0] + p

    try:
        f = open(fn, 'rb')
    except:
        return HttpResponseNotFound("<h1>404 file not found: " + request.path + "</h1>")

    try:
        c = f.read()
    finally:
        f.close()

    ct = "text/plane"
    if fn.count(".ico") > 0:
        ct = "image/vnd.microsoft.icon"
    elif fn.count(".xml") > 0:
        ct = "text/xml"
    elif fn.count(".html") > 0:
        ct = "text/html"
    return HttpResponse(c, content_type=ct)


def algs_not_found(request):
    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    v = dict(cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))

    html = get_template('alg_not_found_%s.html' % alg_detect_lang(request))
    return HttpResponseNotFound(html.render(v))


def algs_start(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]
    last_algs_tag = tag_xml[2]

    html = get_template('alg_start_%s.html' % alg_detect_lang(request))
    v = dict(code=code, user=user,
             cath=get_caths(cath_tags), tag=get_tags(tag_tags),
             last_algs=get_algs(last_algs_tag, 1, 1, 1, "LM"))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_page(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    s_path = request.path.lower().split('/')
    if len(s_path) < 1:
        return HttpResponseRedirect('/')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    c_path = s_path[1]
    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    html = get_template('alg_page_%s_%s.html' % (c_path, alg_detect_lang(request)))
    v = dict(page_title=c_path, code=code, user=user, cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_redirects(request):
    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    return HttpResponseRedirect('/namespace/%s/' % s_path[2])


def algs_namespace_list(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponseRedirect('/')

    group_name = s_path[2]
    if len(group_name) == 0:
        return HttpResponseRedirect('/')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki_cached('q_cath', {'cath': group_name, 'lang': alg_detect_lang(request)}, 120)
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tag = tag_xml[0]

    info = cath_tag.get("info")
    cath_title = None
    if info is None:
        local_tag = xml_search_tag(cath_tag, 'local')
        if local_tag is not None:
            cath_title = local_tag.get("def_value")

    if (cath_title is None) or (len(cath_title) == 0):
        cath_title = from_upper(group_name)

    alg_tags = xml_search_tag(cath_tag, 'algs')

    html = get_template('alg_list_cath_%s.html' % alg_detect_lang(request))
    v = dict(cath_name=group_name, cath_title=cath_title, alg=get_algs(alg_tags, 1),
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_tag_list(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponseRedirect('/')

    tag_name = s_path[2]
    if len(tag_name) == 0:
        return HttpResponseRedirect('/')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki_cached('q_tag', {'tag': tag_name, 'lang': alg_detect_lang(request)}, 120)
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        return algs_db_error_page(request, info)

    tag_tag = tag_xml[0]

    info = tag_tag.get("info")
    tag_title = None
    if info is None:
        local_tag = xml_search_tag(tag_tag, 'local')
        if local_tag is not None:
            tag_title = local_tag.get("def_value")

    if (tag_title is None) or (len(tag_title) == 0):
        tag_title = from_upper(tag_name)

    alg_tags = xml_search_tag(tag_tag, 'algs')

    html = get_template('alg_list_tag_%s.html' % alg_detect_lang(request))
    v = dict(tag_name=tag_name, tag_title=tag_title, alg=get_algs(alg_tags, 1, 1, 1),
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_search(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    search_text = request.POST.get('search').strip()
    if len(search_text) == 0:
        return HttpResponseRedirect('/')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki('q_search',
                             {'name': search_text, 'search': '%%%s%%' % search_text,
                              'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    by_name_tag = tag_xml[0]
    alg_count = axgs_tag_count(by_name_tag)
    if alg_count > 0:
        alg_tag = by_name_tag[0]
        return HttpResponseRedirect('/alg/%s.%s/' % (alg_tag.get("group_name"), alg_tag.get("alg_name")))

    by_descr_tag = tag_xml[1]
    alg_count = axgs_tag_count(by_descr_tag)
    if alg_count == 0:
        html = get_template('alg_search_not_found_%s.html' % alg_detect_lang(request))
        v = dict(search_text=search_text,
                 alg=[],
                 code=code, user=user,
                 cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    else:
        html = get_template('alg_search_%s.html' % alg_detect_lang(request))
        v = dict(search_text=search_text, alg_count=alg_count,
                 code=code, user=user,
                 alg=get_algs(by_descr_tag, 1, 0, 1),
                 cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_view(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    # 1. init
    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponseRedirect('/')

    a_path = s_path[2]
    alg_names = a_path.split('.')
    if len(alg_names) < 2:
        return HttpResponseRedirect('/')

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]

    # 2. rq root info (cats & tags)
    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    # 3. rq alg info
    tag_xml = call_axgs_wiki_cached('q_alg',
                                    {'alg_name': alg_name, 'group_name': group_name,
                                     'lang': alg_detect_lang(request)},
                                    120)
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        return algs_db_error_page(request, info)

    alg_tag = tag_xml[0]
    alg_name2 = alg_tag.get("alg_name")
    if alg_name2 is None:
        alg_name2 = alg_name
    b_is_demo = (group_name == 'demo')
    alg_ret_type = "unk"

    alg_type = alg_tag.get("type_name")

    # 3.2 get alg group info
    # group_name = alg_tag.get("group_name")
    group_title = None
    group_tag = xml_search_tag(alg_tag, 'group_def')
    if group_tag is not None:
        group_title = group_tag.get('def_value')
    if group_title is None:
        group_title = group_name

    # 3.3 get alg defs info
    (alg_title, alg_note, alg_kwords, alg_descr) = extract_def_info_from_alg_tag(alg_tag)

    # 3.4 get alg notation info
    alg_def_notation_tag = None
    alg_def_notation = None
    alg_notations_tag = xml_search_tag(alg_tag, 'notations')
    if alg_notations_tag is not None:
        alg_def_notation_tag = xml_search_tag(alg_notations_tag, 'def_notation')
        if alg_def_notation_tag is not None:
            alg_def_notation = get_alg_notation(alg_name2, alg_type, xml_search_tag(alg_def_notation_tag, 'params'))
    if alg_def_notation is None:
        alg_def_notation = "N/A"

    alg_name_ex = alg_name2 + '()'

    # 4. join with pattern
    v = dict(alg_name=alg_name2,
             alg_title=alg_title, alg_descr=wiki_process(alg_descr), alg_kwords=alg_kwords, alg_note=alg_note,
             alg_name_ex=alg_name_ex, is_demo=b_is_demo,
             group_name=group_name, group_title=group_title,
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1),
             alg_ret_type=alg_ret_type, alg_def_notation=alg_def_notation,
             alg_tags=get_alg_tags(xml_search_tag(alg_tag, 'algtags')),
             alg_params=get_alg_params(xml_search_tag(alg_def_notation_tag, 'params')),
             modules=get_alg_modules(xml_search_tag(alg_def_notation_tag, 'modules'),
                                     xml_search_tag(alg_def_notation_tag, 'def_module'),
                                     xml_search_tag(alg_tag, 'demo_refs')),
             demos=get_alg_demos(xml_search_tag(alg_def_notation_tag, 'demos'))
             )
    html = get_template('alg_view_descr_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_code(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    a_path = s_path[3]
    alg_names = a_path.split('.')
    if len(alg_names) < 2:
        return HttpResponseRedirect('/')

    alg_name = alg_names[len(alg_names) - 1]
    module_name = alg_names[len(alg_names) - 2]
    owner_name = alg_names[len(alg_names) - 3]
    # group_name = '???'

    c_code = ""
    if len(s_path) > 3:
        c_code = s_path[4]

    alg_name_ex = 'func ' + alg_name + '()'

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki('q_module_name', {'name': alg_name, 'module_name': module_name, 'owner_name': owner_name})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_name_tag = tag_xml[0]
    group_name = module_name_tag.get("group_name")

    v = dict(alg_name=alg_name, module_name=module_name, group_name=group_name,
             owner_name=owner_name,
             code=code, user=user,
             alg_name_ex=alg_name_ex,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))

    if c_code == "":
        code = call_axgs_wiki_data('q_alg_code',
                                   {'module_name': module_name, 'owner_name': owner_name, 'alg_name': alg_name})
        if code is None:
            return algs_db_error_page(request, 'code not found')

        if code.find('?xml') > 0:
            tag_xml = axgs_xml_parse(code)
            info = axgs_get_error(tag_xml)
            if info is not None:
                info = 'get source internal error'
            return algs_db_error_page(request, info)

        if (code.find('error') > 0) and (code.find('converter') > 0):
            html = get_template('alg_conv_error_%s.html' % alg_detect_lang(request))
            v.update(report=code)
            return HttpResponse(html.render(v))

        alg_code = unicode(code, 'utf-8')  # cp1251
        html = get_template('alg_view_code_%s.html' % alg_detect_lang(request))
        v.update(alg_code=alg_code, has_source=1)

    elif (c_code == 'pascal') or (c_code == 'python'):

        c_ext = get_ext_by_source_name(c_code)

        code = call_axgs_wiki_data('q_alg_source',
                                   {'module_name': module_name, 'owner_name': owner_name,
                                    'alg_name': alg_name, 'alg_kind': c_code, 'ext': c_ext})
        if code is None:
            return algs_db_error_page(request, 'source not found')

        if code.find('?xml') > 0:
            tag_xml = axgs_xml_parse(code)
            info = axgs_get_error(tag_xml)
            if info is not None:
                return algs_db_error_page(request, info)

        html = get_template('alg_view_source_%s.html' % alg_detect_lang(request))
        alg_code = unicode(code, 'cp1251')
        v.update(alg_code=alg_code)

        if c_code == 'pascal':
            v.update(sel_pas='class=selected')
        if c_code == 'python':
            v.update(sel_py='class=selected')
    else:
        return algs_db_error_page(request, 'unknown code type: ' + c_code)

    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_image(request):
    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponse(status=404, content='404 wrong rq')

    a_path = s_path[3]
    alg_names = a_path.split('.')
    if len(alg_names) < 4:
        return HttpResponse(status=404, content='404 wrong path: '+a_path)

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]
    module_name = alg_names[len(alg_names) - 3]
    owner_name = alg_names[len(alg_names) - 4]

    img = call_axgs_wiki_data('q_alg_img',
                              {'alg_name': alg_name, 'group_name': group_name,
                               'module_name': module_name, 'rel_group': owner_name})
    if (img is None) or (img.count('xml') > 0):
        return HttpResponse(status=500, content='500 wrong image rq: '+a_path)

    return HttpResponse(img, content_type='image/png')


def algs_module_view(request):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    # 1. init
    s_path = request.path.lower().split('/')
    if len(s_path) < 2:
        return HttpResponseRedirect('/')

    m_path = s_path[2]
    module_names = m_path.split('.')
    if len(module_names) < 2:
        return HttpResponseRedirect('/')

    module_name = module_names[len(module_names) - 1]
    owner_name = module_names[len(module_names) - 2]

    # 2. rq root info (cats & tags)
    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki('q_module', {'module': module_name, 'owner': owner_name, 'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        return algs_db_error_page(request, info)

    module_tag = tag_xml[0]

    module_name = module_tag.get("module_name")
    create_date = module_tag.get("create_date")

    names = get_module_names(module_tag)
    has_demo = get_module_demo_status(names)
    has_test = get_module_test_status(names)

    html = get_template('alg_module_view_%s.html' % alg_detect_lang(request))
    v = dict(module_name=module_name, owner_name=owner_name, create_date=create_date,
             code=code, user=user,
             has_test=has_test, has_demo=has_demo,
             names=names,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))
