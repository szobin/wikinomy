#!/usr/bin/python
# -*- coding: utf-8 -*-
from algs.algs_public import algs_db_error_page
from pcode.db_lib import *
from pcode.conf import *
from django.template.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template


def algs_zip_new_params(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    v = dict(code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_zip_new_param_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_zip_new_upload(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/new/module/')

    user = get_safe_session_param(request, 'login')

    # 1. check uploaded file alg
    try:
        zip_data = request.FILES['zip_file']
        if zip_data.size == 0:
            return HttpResponseRedirect('/new/zip/')
    except:
        return HttpResponseRedirect('/new/zip/')

    # 2. read uploaded file alg
    zip_content = zip_data.read()
    zip_fn = zip_data.name

    # 3. transfer and convert uploaded data
    fn = get_upload_path(user, zip_fn)
    f = open(fn, 'wb')
    try:
        f.write(zip_content)
    finally:
        f.close()

    #  (zip_fn2, extension) = os.path.splitext(zip_fn)
    return HttpResponseRedirect('/user/files/')


def algs_zip_preview(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    v = dict(code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_zip_preview_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))
