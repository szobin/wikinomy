#!/usr/bin/python
# -*- coding: utf-8 -*-
from algs.algs_public import algs_db_error_page
from pcode.module_lib import *
from pcode.db_lib import *
from pcode.conf import *
from django.template.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
import base64


def algs_login_form(request, url_next):
    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    info = get_safe_session_param(request, 'info')
    if len(info) > 0:
        del request.session["info"]

    rq_auth = get_safe_session_param(request, 'rq_auth')
    if len(rq_auth) > 0:
        del request.session["rq_auth"]

    html = get_template('alg_login_%s.html' % alg_detect_lang(request))
    v = dict(login=get_safe_session_param(request, 'login'),
             next=url_next, info=info, rq_auth=rq_auth,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_auth(request):
    url_next = get_safe_get_param(request, "next", True)

    code = get_safe_session_param(request, 'code')
    if len(code) == 32:
        if url_next == "":
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect(url_next)

    login = get_safe_post_param(request, "login", True)
    pwd = get_safe_post_param(request, "passw")
    if login == "":
        return algs_login_form(request, url_next)

    url_next = get_safe_post_param(request, "next", True)

    if pwd == "":
        request.session['info'] = 'enter password'
        return algs_login_form(request, url_next)

    request.session['login'] = login
    xml_tag = call_axgs_wiki("q_auth", {'login': login, 'p_hash': encode_pass(pwd)})
    info = axgs_get_error(xml_tag)
    if info is not None:
        request.session['info'] = info
        return algs_login_form(request, url_next)

    code = xml_get_param(xml_tag, "PID")
    request.session['code'] = code
    #  request.session['code']['max-age'] = "7200"

    if url_next == "":
        return HttpResponseRedirect('/user/')
    else:
        return HttpResponseRedirect(url_next)


def algs_logout(request):
    url_next = get_safe_get_param(request, "next", True)

    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        if url_next == "":
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect(url_next)

    del request.session["code"]
    call_axgs_wiki("aq_logout", {})
    if url_next == "":
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect(url_next)


def algs_user(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    return HttpResponseRedirect('/user/files/')


def algs_alg_edit_form(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    a_path = s_path[3]
    alg_names = a_path.split('.')
    if len(alg_names) < 2:
        return HttpResponseRedirect('/')

    op_def = ""
    if len(s_path) > 3:
        op_def = s_path[4]

    if op_def == "":
        return HttpResponseRedirect('/')

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki('q_alg_def',
                             {'alg_name': alg_name, 'group_name': group_name,
                              'def': op_def,
                              'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    info = "alg tag not found"
    alg_tag = xml_search_tag(tag_xml, 'alg_name')
    if alg_tag is not None:
        info = axgs_get_error(alg_tag)
    if info is not None:
        return algs_db_error_page(request, info)

    def_title = from_upper(op_def)
    def_value = None
    defs_tag = xml_search_tag(alg_tag, 'defs')
    if defs_tag is not None:
        for def_tag in defs_tag:
            if def_value is None:
                def_value = def_tag.get("def_value")

    if def_value is None:
        def_value = "--"

    group_name = alg_tag.get("group_name")
    alg_name_ex = 'func '+alg_name+'()'

    v = dict(alg_name=alg_name, def_title=def_title, op_def=op_def, def_value=def_value,
             alg_name_ex=alg_name_ex, group_name=group_name,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1)
             )
    html = get_template('alg_edit_descr_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_store(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    a_path = s_path[3]
    alg_names = a_path.split('.')
    if len(alg_names) < 2:
        return HttpResponseRedirect('/')
    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]

    op_def = ""
    if len(s_path) > 3:
        op_def = s_path[4]

    if op_def == "":
        return HttpResponseRedirect('/')

    def_value = request.POST.get("def_value")
    if def_value is None:
        return algs_db_error_page(request, "text is not defined")
        # return HttpResponseRedirect('/alg/%s.%s/' % (group_name, alg_name))

    tag_xml = call_axgs_wiki('au_def_by_name',
                             {'code': code, 'type_id': "4",
                              'name': alg_name, 'parent_name': group_name,
                              'def': op_def, 'def_value': def_value,
                              'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    return HttpResponseRedirect('/alg/%s.%s/' % (group_name, alg_name))


def algs_alg_edit(request):
    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    op_edit = ""
    if len(s_path) > 4:
        op_edit = s_path[5]

    if op_edit == "":
        return algs_alg_edit_form(request)

    if op_edit == "store":
        return algs_alg_store(request)

    return HttpResponseRedirect('/')


def algs_alg_demo_form(request, alg_names):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]
    module_name = alg_names[len(alg_names) - 3]
    owner_name = alg_names[len(alg_names) - 4]

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    tag_xml = call_axgs_wiki('q_alg',
                             {'alg_name': alg_name, 'group_name': group_name,
                              'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    alg_tag = tag_xml[0]
    info = axgs_get_error(alg_tag)
    if info is not None:
        return algs_db_error_page(request, info)

    alg_id = alg_tag.get("alg_id")
    alg_name = alg_tag.get("alg_name")
    alg_title = get_alg_def(alg_tag, "title", alg_detect_lang(request))

    group_name = alg_tag.get("group_name")
    alg_name_ex = 'func '+alg_name+'()'

    alg_notation_id = None
    alg_def_notation_tag = None
    alg_notations_tag = xml_search_tag(alg_tag, 'notations')
    if alg_notations_tag is not None:
        alg_def_notation_tag = xml_search_tag(alg_notations_tag, 'def_notation')
        if alg_def_notation_tag is not None:
            alg_notation_id = alg_def_notation_tag.get('default_notation_id')

    html = get_template('alg_demo_form_%s.html' % alg_detect_lang(request))
    v = dict(alg_name=alg_name, group_name=group_name,
             code=code, user=user,
             module_name=module_name, owner_name=owner_name,
             alg_title=alg_title, alg_name_ex=alg_name_ex, alg_id=alg_id,
             alg_notation_id=alg_notation_id,
             alg_rels=get_alg_rels(alg_def_notation_tag),
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_demo_exec(request, alg_names):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]
    module_name = alg_names[len(alg_names) - 3]
    owner_name = alg_names[len(alg_names) - 4]

    rel_module_id = get_safe_post_param(request, "rel_id")
    if rel_module_id == "":
        return HttpResponseRedirect('/demo/alg/%s.%s.%s.%s/' %
                                    (owner_name, module_name, group_name, alg_name))

    tag_xml = call_axgs_wiki('q_alg',
                             {'alg_name': alg_name, 'group_name': group_name})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    alg_tag = tag_xml[0]
    info = axgs_get_error(alg_tag)
    if info is not None:
        return algs_db_error_page(request, info)

    alg_def_notation_tag = None
    alg_notations_tag = xml_search_tag(alg_tag, 'notations')
    if alg_notations_tag is not None:
        alg_def_notation_tag = xml_search_tag(alg_notations_tag, 'def_notation')

    if alg_def_notation_tag is None:
        return algs_db_error_page(request, "def notation tag not found")

    alg_rels = get_alg_rels(alg_def_notation_tag)
    alg_rel = get_alg_rel_by_module_id(alg_rels, rel_module_id)
    if alg_rel is None:
        return algs_db_error_page(request, "alg_rel not found id="+rel_module_id)

    rel_name = alg_rel[1]
    rel_group = alg_rel[2]

    rel_module = alg_rel[3]
    rel_owner = alg_rel[4]

    project_name = alg_rel[7]
    project_alias = owner_name+'_'+module_name+'_'+group_name+'_'+alg_name

    axgs_clear_cache()
    tag_xml = call_axgs_wiki('ae_demo',
                             {'code': code,
                              'name': alg_name, 'group_name': group_name,
                              'module_name': module_name, 'owner_name': owner_name,
                              'rel_name': rel_name, 'rel_group': rel_group,
                              'rel_module_name': rel_module, 'rel_owner': rel_owner})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    names = list()
    names.append(['wikinomy.'+group_name+'.'+alg_name, owner_name+'.'+module_name])
    names.append(['wikinomy.'+rel_group+'.'+rel_name, rel_owner+'.'+rel_module])

    wsa_config = make_wsa_config(dict(project_name=rel_group+'.'+project_name,
                                      project_alias=project_alias,
                                      names=names,
                                      complile='delphi'))

    report = call_axgs_wiki_data('a_demo_make',
                                 {'code': code, 'wsa_config': base64.b64encode(wsa_config),
                                  'project_alias': project_alias})
    if len(report) == 0:
        return algs_db_error_page(request, 'a_demo_make: internal error')

    if report.count('?xml') > 0:
        tag_xml = axgs_xml_parse(report)
        info = axgs_get_error(tag_xml)
        if info is not None:
            return algs_db_error_page(request, "make demo code error: "+info)
        return algs_db_error_page(request, report)

    if (report.count('error') > 0) or (report.count('Error') > 0) or (report.count('Fatal') > 0):
        html = get_template('alg_demo_info_%s.html' % alg_detect_lang(request))
        v = dict(code=code, user=user, alg_name=alg_name, module_name=module_name, group_name=group_name)
        v.update(report=report)
        v.update(csrf(request))
        return HttpResponse(html.render(v))

    tag_xml = call_axgs_wiki('a_demo_run',
                             {'code': code, 'project_alias': project_alias})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    return HttpResponseRedirect('/demo/alg/%s.%s.%s.%s/view/%s.%s.%s.%s/' %
                                (owner_name, module_name, group_name, alg_name,
                                 rel_owner, rel_module, rel_group, rel_name))


def algs_alg_demo_view(request, alg_names, rel_names):
    code = get_safe_session_param(request, 'code')
    user = get_safe_session_param(request, 'login')

    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]
    module_name = alg_names[len(alg_names) - 3]
    owner_name = alg_names[len(alg_names) - 4]

    alg_name_ex = 'func '+alg_name+'()'

    if len(rel_names) < 4:
        return HttpResponseRedirect('/')

    rel_name = rel_names[len(rel_names) - 1]
    rel_group = rel_names[len(rel_names) - 2]
    rel_module = rel_names[len(rel_names) - 3]
    rel_owner = rel_names[len(rel_names) - 4]

    html = get_template('alg_demo_view_%s.html' % alg_detect_lang(request))
    v = dict(code=code, user=user,
             alg_name=alg_name, group_name=group_name,
             module_name=module_name, owner_name=owner_name,
             alg_name_ex=alg_name_ex,
             rel_name=rel_name, rel_group=rel_group,
             rel_module_name=rel_module, rel_owner=rel_owner)
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_alg_demo_mark_ok(request, alg_names):
    code = get_safe_session_param(request, 'code')

    alg_name = alg_names[len(alg_names) - 1]
    group_name = alg_names[len(alg_names) - 2]
    module_name = alg_names[len(alg_names) - 3]
    owner_name = alg_names[len(alg_names) - 4]

    if len(code) != 32:
        return HttpResponseRedirect('/demo/alg/%s.%s.%s.%s/' % (owner_name, module_name, group_name, alg_name))

    rel_name = request.POST.get("rel_name")
    rel_group = request.POST.get("rel_group")
    rel_module_name = request.POST.get("rel_module_name")
    rel_owner = request.POST.get("rel_owner")
    if (rel_name is None) or (rel_group is None) or \
       (rel_module_name is None) or (rel_owner is None):
        return algs_db_error_page(request, "mark params were not defined")

    axgs_clear_cache()
    tag_xml = call_axgs_wiki('au_tested',
                             {'code': code,
                              'name': alg_name, 'group_name': group_name,
                              'module_name': module_name, 'owner_name': owner_name,
                              'rel_name': rel_name, 'rel_group': rel_group,
                              'rel_module_name': rel_module_name, 'rel_owner': rel_owner,
                              'is_tested': "1"})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    return HttpResponseRedirect('/alg/%s.%s/' % (group_name, alg_name))


def algs_demo_alg(request):
    s_path = request.path.lower().split('/')
    if len(s_path) < 3:
        return HttpResponseRedirect('/')

    a_path = s_path[3]
    alg_names = a_path.split('.')
    if len(alg_names) < 2:
        return HttpResponseRedirect('/')

    op_edit = ""
    if len(s_path) > 3:
        op_edit = s_path[4]

    if op_edit == "":
        return algs_alg_demo_form(request, alg_names)

    if op_edit == "exec":
        return algs_alg_demo_exec(request, alg_names)

    if op_edit == "view":
        if len(s_path) < 5:
            return HttpResponseRedirect('/')

        d_path = s_path[5]
        demo_names = d_path.split('.')
        return algs_alg_demo_view(request, alg_names, demo_names)

    if op_edit == "mark_ok":
        return algs_alg_demo_mark_ok(request, alg_names)

    return HttpResponseRedirect('/')


def algs_clear_cache(request):
    axgs_clear_cache()
    return HttpResponseRedirect('/user/')


def algs_user_files(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    path = upload_path(user)
    names = make_path_names(path)

    v = dict(names=names,
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_user_files_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_user_file_view(request, fn, fn_ext):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')
    path = upload_path(user)
    full_fn = make_fill_fn(path, fn)+'.'+fn_ext

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    zip_names = module_zip_names(full_fn)
    v = dict(names=zip_names,
             code=code, user=user, fn=fn+'.'+fn_ext,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_user_file_view_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_user_file_proc(request, fn, fn_ext):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')
    path = upload_path(user)
    full_fn = make_fill_fn(path, fn)+'.'+fn_ext
    report = module_proc_zip_file(code, user, full_fn)
    report_errors = module_report_errors(report)

    #  if module_report_errors(report) == 0:
    #    return HttpResponseRedirect('/user/files/')

    v = dict(report=report, report_errors=report_errors,
             code=code, user=user, fn=fn+'.'+fn_ext,
             )
    html = get_template('alg_user_file_proc_report_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_user_file_del(request, fn, fn_ext):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')
    path = upload_path(user)
    full_fn = make_fill_fn(path, fn)+'.'+fn_ext

    os.remove(full_fn)
    return HttpResponseRedirect('/user/files/')


def algs_user_modules(request):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    tag_xml = call_axgs_wiki_cached('aq_user_modules', {'Code': code})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    modules = make_user_module_list(tag_xml)

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    v = dict(modules=modules,
             code=code, user=user,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_user_modules_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_user_module_view(request, module_name):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    code_xml = call_axgs_wiki('q_module_xml', {'module_name': module_name, 'owner_name': user})
    info = axgs_get_error(code_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_info = get_module_info(code_xml)
    module_code_cells = get_module_cells(code_xml)

    group_name = module_info["namespace"]

    tag_xml = call_axgs_wiki('q_module', {'module': module_name, 'owner': user, 'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        return algs_db_error_page(request, info)

    module_tag = tag_xml[0]

    module_name = module_tag.get("module_name")
    create_date = module_tag.get("create_date")

    module_wiki_names = get_module_names(module_tag)

    names = module_join_names_and_funcs(module_code_cells, module_wiki_names)

    tag_xml = call_axgs_wiki_cached('q_root', {'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    cath_tags = tag_xml[0]
    tag_tags = tag_xml[1]

    v = dict(names=names, namespace=group_name,
             code=code, user=user, module_name=module_name, create_date=create_date,
             cath=get_caths(cath_tags, 1), tag=get_tags(tag_tags, 1))
    html = get_template('alg_user_module_view_%s.html' % alg_detect_lang(request))
    v.update(csrf(request))
    return HttpResponse(html.render(v))


def algs_user_module_cell_publicate(request, module_name, element_name):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    code_xml = call_axgs_wiki('q_module_xml', {'module_name': module_name, 'owner_name': user})
    info = axgs_get_error(code_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_info = get_module_info(code_xml)
    module_funcs = get_module_cells(code_xml)

    group_name = module_info["namespace"]
    rq_nss = module_info['required_nss']
    if rq_nss is None:
        rq_nss = ''

    wiki_names = get_wiki_names(rq_nss)

    module_func = module_get_name(module_funcs, element_name)
    if module_func is None:
        return algs_db_error_page(request, "element %s not found in module %s" % (element_name, module_name))

    tag_xml = call_axgs_wiki('q_module', {'module': module_name, 'owner': user, 'lang': alg_detect_lang(request)})
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        return algs_db_error_page(request, info)

    module_tag = tag_xml[0]

    module_name = module_tag.get("module_name")
    module_names = get_module_names(module_tag)
    if module_get_name(module_names, element_name) is not None:
        return algs_db_error_page(request, "%s.%s already publicated" % (module_name, element_name))

    (project_group, project_name) = get_project_params(module_info['project'], "")
    if project_group == '':
        project_group = group_name

    project_id = None
    if (project_name is not None) and (len(project_name) > 0):
        # 8.1 update project group info
        (group_id, info) = do_update_group_info(code, project_group, "")
        if group_id is None:
            return algs_db_error_page(request, "*%s.%s module update project namespace %s error: %s" %
                                      (user, module_name, project_group, info))

        (project_id, info) = do_update_project_info(code, project_name, group_id)
        if project_id is None:
            return algs_db_error_page(request, "*%s.%s module update project ref error: %s" % (user, module_name, info))

    (name_id, info) = do_publicate_name(code, group_name, user, module_name, module_func, project_id, wiki_names)
    if info is not None:
        return algs_db_error_page(request, "%s.%s publication error: %s" % (module_name, element_name, info))

    return HttpResponseRedirect('/user/module/%s/view/' % module_name)


def algs_user_module_cell_cancel(request, module_name, element_name):
    code = get_safe_session_param(request, 'code')
    if len(code) != 32:
        request.session['rq_auth'] = '1'
        return HttpResponseRedirect('/auth/?next=%s' % request.path)

    user = get_safe_session_param(request, 'login')

    code_xml = call_axgs_wiki('q_module_xml', {'module_name': module_name, 'owner_name': user})
    info = axgs_get_error(code_xml)
    if info is not None:
        return algs_db_error_page(request, info)

    module_info = get_module_info(code_xml)
    module_funcs = get_module_cells(code_xml)

    group_name = module_info["namespace"]

    module_func = module_get_name(module_funcs, element_name)
    if module_func is None:
        return algs_db_error_page(request, "element %s not found in module %s" % (element_name, module_name))

    (name_id, info) = do_cancel_publicate_name(code, group_name, user, module_name, module_func)
    if info is not None:
        return algs_db_error_page(request, "%s.%s publication cancel error: %s" % (module_name, element_name, info))

    return HttpResponseRedirect('/user/module/%s/view/' % module_name)
