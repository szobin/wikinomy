"""  wikinomy URL Configuration
"""

from algs.algs_download import *
#  from algs.algs_descr import *
from algs.algs_module import *
from algs.algs_zip import *
from algs.algs_private import *
from algs.algs_public import *
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
# from django.contrib import admin

handler404 = algs_not_found

urlpatterns = [
                url(r'^robots.txt$', algs_static),
                url(r'^favicon.ico$', algs_static),
                url(r'^sitemap.xml$', algs_static),
                url(r'^\w+.html$', algs_static),
                url(r'^about/', algs_page),

                url(r'^$', algs_start),
                url(r'^search/', algs_alg_search),

                url(r'^cath/', algs_redirects),
                url(r'^thema', algs_redirects),

                url(r'^namespace/', algs_namespace_list),
                url(r'^tag/', algs_tag_list),
                url(r'^module/', algs_module_view),
                url(r'^alg/', algs_alg_view),

                url(r'^image/alg/', algs_alg_image),
                url(r'^code/alg/', algs_alg_code),

                url(r'^auth/$', algs_auth),
                url(r'^logout/$', algs_logout),

                url(r'^user/clearcache/$', algs_clear_cache),
                url(r'^user/file/(\w+).(\w+)/view/$', algs_user_file_view),
                url(r'^user/file/(\w+).(\w+)/proc/$', algs_user_file_proc),
                url(r'^user/file/(\w+).(\w+)/del/$', algs_user_file_del),
                url(r'^user/files/$', algs_user_files),
                url(r'^user/module/(\w+)/view/$', algs_user_module_view),
                url(r'^user/module/(\w+).(\w+)/publicate/$', algs_user_module_cell_publicate),
                url(r'^user/module/(\w+).(\w+)/cancel/$', algs_user_module_cell_cancel),
                url(r'^user/modules/$', algs_user_modules),
                url(r'^user/$', algs_user),

                url(r'^edit/alg/', algs_alg_edit),

                url(r'^new/zip/upload/', algs_zip_new_upload),
                url(r'^new/zip/$', algs_zip_new_params),

                url(r'^new/module/upload/', algs_module_new_upload),
                url(r'^new/module/preview/', algs_module_new_preview),
                url(r'^new/module/insert/', algs_module_new_insert),
                url(r'^new/module/$', algs_module_new),

                #  url(r'^new/descr/upload/', algs_descr_new_upload),
                #  url(r'^new/descr/preview/', algs_descr_new_preview),
                #  url(r'^new/descr/insert/', algs_descr_new_insert),
                #  url(r'^new/descr/$', algs_descr_new),

                url(r'^demo/alg/', algs_demo_alg),

                url(r'^download/module/', algs_download_module),
                url(r'^code/delphi/', algs_code_delphi),
                url(r'^code/cpp/', algs_code_cpp),

    # url(r'^admin/', admin.site.urls),
              ]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)