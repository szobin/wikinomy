#!/usr/bin/python
# -*- coding: utf-8 -*-

from wikinomy.settings import AXGSconfig
from lxml import etree
from wikinomy import settings
import requests
import urllib
import os
import time


def xml_get_param(xml_tag, name):
    value = xml_tag.get(name)
    if value is None:
        value = ""
    return value


def decode_x(i_str):
    return unicode(i_str, 'cp1251').encode('cp866')


def xml_get_param_ex(xml_tag, name, include_tag_name=0):
    value = xml_tag.get(name)
    if value is not None:
        if include_tag_name > 0:
            return xml_tag.tag+'.'+name+':'+value
        else:
            return value

    for subtag in xml_tag:
        value = xml_get_param_ex(subtag, name)
        if value is not None:
            if include_tag_name > 0:
                return subtag.tag+'.'+name+':'+value
            else:
                return value
    return None


def url_ruspar(s):
    uu = s.encode('cp1251')
    uuu = ""
    for c in uu:
        w = hex(ord(c)).upper()
        uuu += "%" + w[2:4]
    return uuu


def axgs_get_error(xml_tag, level=1):
    # 0
    info = xml_get_param_ex(xml_tag, "error", 1)

    # 1
    if level == 1:
        if info is None:
            info = xml_tag.get("info")
    # 2
    if level == 2:
        if info is None:
            info = xml_get_param_ex(xml_tag, "info")

    # 3
    if level == 3:
        if info is None:
            if len(xml_tag) > 0:
                info = xml_tag[0].get("info")

    return info



def axgs_xml_parse(xmlstr):
    if xmlstr.find('xml') < 0:
        xml = etree.Element("axgs")
        tag_attr = xml.attrib
        tag_attr["info"] = "no xml return"
        return xml

    try:
        # parser = etree.XMLParser(encoding='cp1251')
        # xml = etree.fromstring(xmlstr, parser)
        xml = etree.fromstring(xmlstr)
    except Exception as ex:
        xml = etree.Element("axgs")
        tag_attr = xml.attrib
        tag_attr["info"] = "xml parse error: " + str(ex) + " xml:" + xmlstr
    return xml


def xml_str_param(tag, name, default=""):
    if tag is None:
        return default

    try:
        v = tag.get(name)
        if v is None:
            return default
        if v == "":
            return default
        return v
    except:
        return default


def axgs_tag_count(root_tag):
    n = 0
    for tag in root_tag:
        s = xml_str_param(tag, 'info', '') + xml_str_param(tag, 'error', '')
        if len(s) > 0:
            continue
        n += 1
    return n


def call_axgs_post_ex(host, port, page, params):
    # host = "localhost"
    if host == "":
        return '<?xml version="1.0" ?><axgs info="service host value empty "/>'

    url = "http://" + host + ":" + port + "/" + page
    e_params = dict()
    for (name, value) in params.items():
        if value is None:
            continue
        if not value.isdigit():
            value = value.encode('cp1251')
        e_params[name] = value

    try:
        r = requests.post(url, data=e_params, timeout=(30, 1200))
        if r.status_code == 200:
            content = r.content
        else:
            content = '<?xml version="1.0" ?><axgs info="service post error #' + str(
                    r.status_code) + ':' + r.reason + ' act: ' + params['act'] + '"/>'
    except requests.ConnectionError as ex:
        info = str(ex.args[0])
        content = '<?xml version="1.0" ?><axgs info="connection error: ' + urllib.quote(info) + '"/>'

    except Exception as ex:
        content = '<?xml version="1.0" ?><axgs info="service post raise: ' + urllib.quote(str(ex)) + '"/>'
    return content


def get_axgs_fn_cache(act, params):
    db = AXGSconfig['db']
    r = act
    for (name, value) in params.items():
        r += '_'+name+'_'+value
    return os.path.join(settings.BASE_DIR, 'cache', db, r)


def axgs_clear_cache():
    db = AXGSconfig['db']
    p = os.path.join(settings.BASE_DIR, 'cache', db)
    files = os.listdir(p)
    for c_file in files:
        ff = os.path.join(p, c_file)
        os.remove(ff)
    return


def get_axgs_cache(act, params, max_age):
    fn = get_axgs_fn_cache(act, params)
    try:
        stat = os.stat(fn)
    except:
        return None

    dt = time.time() - stat.st_atime
    if dt > max_age:
        try:
            os.remove(fn)
        except:
            pass
        return None

    try:
        f = open(fn, 'rb')
    except:
        return None

    try:
        c = f.read()
    finally:
        f.close()
    return c


def set_axgs_cache(act, params, resp):
    fn = get_axgs_fn_cache(act, params)
    try:
        f = open(fn, 'wb')
    except:
        return
    try:
        f.write(resp)
    finally:
        f.close()
    return


def call_axgs_wiki_data(act, params):
    # host = 'dl.uenet.biz'
    host = AXGSconfig['host']
    port = AXGSconfig['port']
    db = AXGSconfig['db']

    params_ex = {'act': act}
    params_ex.update(params)
    resp = call_axgs_post_ex(host, port, db, params_ex)
    return resp


def call_axgs_wiki(act, params):
    return axgs_xml_parse(call_axgs_wiki_data(act, params))


def call_axgs_wiki_cached(act, params, max_age=7200):
    resp = get_axgs_cache(act, params, max_age)
    if resp is not None:
        return axgs_xml_parse(resp)

    resp = call_axgs_wiki_data(act, params)
    xml_tag = axgs_xml_parse(resp)
    info = axgs_get_error(xml_tag)
    if info is None:
        set_axgs_cache(act, params, resp)
    return xml_tag


def get_safe_session_value(request, name):
    value = ""
    if name in request.session:
        value = request.session[name]
    if value is None:
        value = ""
    return value
