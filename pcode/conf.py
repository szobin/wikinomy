#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.conf import settings
import os


def temp_path(fn):
    path = os.path.join(settings.BASE_DIR, 'temp')
    if not os.path.exists(path):
        os.makedirs(path)
    return os.path.join(path, fn)


def upload_path(user):
    path = os.path.join(settings.BASE_DIR, 'uploads')
    if not os.path.exists(path):
        os.makedirs(path)
    path =  os.path.join(path, user)
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def get_upload_path(user, fn):
    return os.path.join(upload_path(user), fn)
