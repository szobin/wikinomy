#!/usr/bin/python
# -*- coding: utf-8 -*-

# from pcode.translit import *
import hashlib
import os
from datetime import datetime
from pytils import translit


def xml_search_tag(xml_tag, name):
    if xml_tag is None:
        return None

    if xml_tag.tag == name:
        return xml_tag

    for sub_tag in xml_tag:
        if sub_tag.tag == name:
            return sub_tag
    return None


def xml_get_value_by_path(xml_tag, path):
    names = path.split('.')
    n = len(names)
    if n == 0:
        return ""

    tag = xml_tag
    if tag.tag == names[0]:
        del names[0]
        n = len(names)

    for name in names:
        if tag is None:
            return ""

        if n <= 1:
            v = tag.get(name)
            if v is None:
                v = ""
            return v

        tag = xml_search_tag(tag, name)
        n -= 1
    return ""


def get_safe_get_param(request, value, is_lower=False):
    try:
        v = request.GET.get(value)
        if v is None:
            v = ""
        if is_lower and (v is not None) and (len(v) > 0):
            v = v.lower()
    except:
        v = ""
    return v


def get_safe_post_param(request, value, is_lower=False):
    try:
        v = request.POST.get(value)
        if v is None:
            v = ""
        if is_lower and (v is not None) and (len(v) > 0):
            v = v.lower()
    except:
        v = ""
    return v


def get_safe_session_param(request, name):
    value = ""
    if name in request.session:
        value = request.session[name]
    if value is None:
        value = ""
    return value


def get_safe_param(param_dict, param_name, def_value=None):
    if param_name in param_dict:
        return param_dict[param_name]
    else:
        return def_value


def encode_pass(v):
    h = hashlib.md5()
    h.update(v)
    return h.hexdigest()


def alg_detect_lang(request):
    host = request.get_host()
    lang = 'en'
    if (host is not None) and (host.count('.ru') > 0):
        lang = 'ru'

    if (host is not None) and (host.count('127.0.0.1') > 0):
        lang = 'ru'

    # LANGUAGE_CODE = lang
    return lang


def insert_lang_to_fn(fn, lang):
    p = fn.find('.')
    if p < 0:
        return fn+'_'+lang
    else:
        return fn[:p]+'_'+lang+fn[p:]


def from_upper(s):
    if (s is None) or (len(s) == 0):
        return ""
    u = u'' + s
    r = u[0:1].upper() + u[1:].lower()
    return r


def divider(n, dd=3):
    nn = round((1+float(n)) / dd)
    if nn <= 1:
        return 1
    return nn


def get_ns_list(ns_tags):
    ns_list = list()
    for ns_tag in ns_tags:
        info = ns_tag.get("info")
        if info is not None:
            continue

        ns_name = ns_tag.get("group_name")
        tag_local = ns_tag[0]
        ns_title = tag_local.get("def_value")
        if (ns_title is None) or (len(ns_title) == 0):
            ns_title = from_upper(ns_name)
        ns_list.append([ns_title, ns_name])
    ns_list.sort()
    return ns_list


def get_caths(ns_tags, col_cnt=3):
    ns_list = get_ns_list(ns_tags)
    return get_li_columns(ns_list, '<a href="/namespace/%s/">%s</a>', col_cnt)


def get_tag_list(tag_tags):
    tag_list = list()
    for tag_tag in tag_tags:
        info = tag_tag.get("info")
        if info is not None:
            continue
        tag_local = tag_tag[0]
        tag_name = tag_tag.get("tag_name")
        tag_title = tag_local.get("def_value")
        if (tag_title is None) or (len(tag_title) == 0):
            tag_title = tag_name

        tag_list.append([tag_title, tag_name])
    tag_list.sort()
    return tag_list


def get_li_columns(cell_list, pattern, col_cnt):
    n3 = divider(len(cell_list))
    if col_cnt == 1:
        li_columns = ""
    else:
        li_columns = ["", "", ""]

    i = 0
    x = 0
    for cell_item in cell_list:
        s = pattern % (cell_item[1], cell_item[0])
        s = '<li>'+s+'</li>'
        if col_cnt == 1:
            li_columns += s
        else:
            li_columns[i] += s
        x += 1
        if (x >= n3) and (i < 2):
            i += 1
            x = 0
    return li_columns


def get_tags(tag_tags, col_cnt=3):
    tag_list = get_tag_list(tag_tags)
    return get_li_columns(tag_list, '<a href="/tag/%s/">%s</a>', col_cnt)


def get_alg_tag_link_str(alg_tag):
    tag_str = ""

    alg_tags = xml_search_tag(alg_tag, 'algtags')
    if alg_tags is None:
        return tag_str

    nn = 0
    for tag_tag in alg_tags:
        info = tag_tag.get('info')
        if info is not None:
            continue
        if nn > 0:
            tag_str += ', '
        tag_name = tag_tag.get('tag_name')
        tag_title = None

        tag_local = xml_search_tag(tag_tag, 'local')
        if tag_local is not None:
            tag_title = tag_local.get('def_value')

        if (tag_title is None) or (len(tag_title) == 0):
            tag_title = tag_name

        tag_str += '<a href="/tag/%s/">%s</a>' % (tag_name, tag_title)
        nn += 1

    return tag_str


def extract_date(s):
    p = s.find(" ")
    if p >= 0:
        s = s[:p]
    return s


def get_algs(alg_tags, row_cnt=3, has_local=1, show_group=0, date_field=""):

    n3 = divider(len(alg_tags))
    if row_cnt == 1:
        algs = ""
    else:
        algs = ["", "", ""]
    i = 0
    x = 0
    for alg_tag in alg_tags:
        info = alg_tag.get("info")
        if info is not None:
            continue

        # alg title
        if has_local == 1:
            group_name = alg_tag.get("group_name")
            alg_name = alg_tag.get("alg_name")

            (alg_title, alg_note, alg_kw, alg_descr) = extract_def_info_from_alg_tag(alg_tag)

        else:
            tag_alg_info = xml_search_tag(alg_tag, 'alg')
            alg_name = tag_alg_info.get("alg_name")
            group_name = tag_alg_info.get("group_name")

            (alg_title, alg_note, alg_kw, alg_descr) = extract_def_info_from_alg_tag(alg_tag)

        if (alg_title is None) or (len(alg_title) == 0):
            alg_title = from_upper(alg_name)

        if alg_note is None:
            alg_note = ""

        if date_field == "":
            d = ""
        else:
            d = extract_date(alg_tag.get(date_field))+" "

        if show_group == 1:
            s = '<li>%s<a href="/alg/%s.%s/">%s.%s</a> | %s ' % \
                (d, group_name, alg_name, group_name, alg_name, alg_title)
        else:
            s = '<li>%s<a href="/alg/%s.%s/">%s</a> | %s ' % \
                (d, group_name, alg_name, alg_name, alg_title)
        s += '<p>'
        tag_str = get_alg_tag_link_str(alg_tag)

        # alg note
        if len(alg_note) > 0 and len(tag_str) > 0:
            s += alg_note + '<br>' + tag_str
        elif len(alg_note) > 0:
            s += alg_note
        else:
            s += tag_str
        s += '</p>'

        if row_cnt == 1:
            algs += s
        else:
            algs[i] += s

        x += 1
        if (x >= n3) and (i < 2):
            i += 1
            x = 0
    return algs


def get_alg_params(alg_params_tag):
    alg_params = []
    if alg_params_tag is None:
        # alg_params.append([0, "debug: params tag - None"])
        return alg_params

    nn = 0
    for param_tag in alg_params_tag:
        info = param_tag.get("info")
        if info is not None:
            continue

        nn += 1
        param_name = param_tag.get('param_name')
        param_type = param_tag.get('type_name')
        param_descr = None

        if len(param_tag) > 0:
            tag_local = param_tag[0]
            param_descr = tag_local.get('def_value')
        if (param_descr is None) or (len(param_descr) == 0):
            param_descr = "--"

        alg_params.append([nn, param_name, param_type, param_descr])
    return alg_params


def get_alg_tags(alg_tags):
    tags = []
    if alg_tags is None:
        return tags
    nn = 0
    for alg_tag in alg_tags:
        info = alg_tag.get("info")
        if info is not None:
            continue

        nn += 1
        tag_name = alg_tag.get('tag_name')
        tag_title = None

        if len(alg_tag) > 0:
            tag_local = alg_tag[0]
            tag_title = tag_local.get('def_value')
        if (tag_title is None) or (len(tag_title) == 0):
            tag_title = tag_name

        tags.append([nn, tag_name, tag_title])
    return tags


def get_alg_tag_str(alg_tags):
    tags = get_alg_tags(alg_tags)
    s = ""
    for tag in tags:
        if len(s) > 0:
            s += ', '
        s += tag[2]
    return s


def get_demo_ref_tag(demo_refs, module_id):
    for demo_ref_tag in demo_refs:
        info = demo_ref_tag.get("info")
        if info is not None:
            continue

        if demo_ref_tag.get("rel_module_id") == module_id:
            return demo_ref_tag

    return None


def get_alg_modules(alg_modules, def_module=None, demo_refs=None):
    modules = []
    if alg_modules is None:
        return modules

    def_module_id = ""
    if def_module is not None:
        def_module_id = def_module.get('default_module_id')
        if def_module_id is None:
            def_module_id = ""
    nn = 0
    for module_tag in alg_modules:
        info = module_tag.get("info")
        if info is not None:
            continue

        nn += 1
        is_def = ""
        module_id = module_tag.get('module_id')
        if len(def_module_id) > 0:
            if def_module_id == module_id:
                is_def = "*"

        ref_alg_name = ""
        ref_module_name = ""
        if demo_refs is not None:
            demo_ref_tag = get_demo_ref_tag(demo_refs, module_id)
            if demo_ref_tag is not None:
                ref_alg_name = demo_ref_tag.get("group")+'.'+demo_ref_tag.get("alg_name")
                ref_module_name = demo_ref_tag.get("owner_name")+'.'+demo_ref_tag.get("module_name")

        is_tested = ""
        if module_tag.get('is_tested') == "True":
            is_tested = "1"

        modules.append([nn,  # 0
                        module_tag.get('module_name'),  # 1
                        module_tag.get('create_date'),  # 2
                        module_tag.get('owner_name'),  # 3
                        module_tag.get('group_name'),   # 4
                        is_tested,    # 5
                        is_def,  # 6
                        ref_alg_name,  # 7
                        ref_module_name  # 8
                        ])
    return modules


def get_alg_demos(alg_demos):
    demos = []
    if alg_demos is None:
        return demos

    nn = 0
    for demo_tag in alg_demos:
        info = demo_tag.get('info')
        if info is not None:
            continue

        nn += 1
        demos.append([nn,  # 0
                      demo_tag.get('name_rel'),  # 1
                      demo_tag.get('group_rel'),  # 2
                      demo_tag.get('name_rel_id'),  # 3
                      demo_tag.get('rel_module_name'),  # 4
                      demo_tag.get('rel_owner_name'),  # 5
                      demo_tag.get('rel_module_id'),  # 6
                      demo_tag.get('demo_project_name'),  # 7
                      demo_tag.get('demo_project_id')  # 8
                      ])
    return demos


def get_alg_notation(name, type_name, params_tag):
    notation = type_name+" "+name
    if params_tag is None:
        return notation

    notation += "("
    nn = 0
    for param_tag in params_tag:
        info = param_tag.get("info")
        if info is not None:
            continue

        param_name = param_tag.get("param_name")
        type_name = param_tag.get("type_name")
        if param_name == "_self_":
            notation = type_name + " " + notation
            continue
        if nn > 0:
            notation += ", "
        notation += param_name+": "+type_name
        nn += 1

    notation += ")"

    return notation


def get_data_type_letter(data_type):
    if (data_type == 'integer') or (data_type == 'int'):
        return "I"
    if data_type == 'float':
        return "F"
    if (data_type == 'string') or (data_type == 'str'):
        return "S"
    if (data_type == '') or (data_type == 'void'):
        return "V"
    if (data_type == 'boolean') or (data_type == 'bool'):
        return "L"
    return "P"


def get_alg_notation_signature(func_type, func_params):
    notation = "r"+get_data_type_letter(func_type.lower())
    nn = 0
    for param in func_params:
        param_type = param[1]
        notation += "_i"+get_data_type_letter(param_type)
        nn += 1
    return notation


def get_module_demo_status(names):
    s = ""
    for name in names:
        if name[5] == "":
            continue

        s = "1"
        break
    return s


def get_module_test_status(names):
    s = ""
    for name in names:
        if name[6] == "":
            continue

        s = "1"
        break
    return s


def get_module_names(module_tag):
    names = []
    if module_tag is None:
        return names

    nn = 0
    for name_tag in module_tag:
        info = name_tag.get('info')
        if info is not None:
            continue

        nn += 1

        type_name = name_tag.get('type_name')
        if type_name is None:
            type_name = '??'

        name = name_tag.get('name')
        if name is None:
            name = '??'
        group_name = name_tag.get('group_name')

        params_tag = xml_search_tag(name_tag, "params")

        notation = get_alg_notation(name, type_name, params_tag)
        name_rels = get_alg_rels(name_tag)
        is_tested = name_tag.get('is_tested')
        if is_tested == "True":
            is_tested = "1"
        else:
            is_tested = ""

        is_demo = ""
        if group_name == "demo":
            is_demo = "1"

        names.append([nn,  # 0
                      name,  # 1
                      group_name,  # 2
                      notation,  # 3
                      type_name,  # 4
                      is_demo,  # 5
                      is_tested,  # 6
                      name_rels  # 7
                      ])
    return names


def get_module_demo_tag(module_tag, prog_lang_id):
    if module_tag is None:
        return None
    for demo_tag in module_tag:
        info = demo_tag.get("info")
        if info is not None:
            continue

        p_id = demo_tag.get("prog_lang_id")
        if p_id == prog_lang_id:
            return demo_tag
    return None


def get_module_packets(prog_langs_tag, module_tag):
    packets = []
    nn = 0
    for prog_lang_tag in prog_langs_tag:
        info = prog_lang_tag.get("info")
        if info is not None:
            break

        nn += 1
        passed_count = ""
        download_count = ""
        prog_lang_id = prog_lang_tag.get("prog_lang_id")
        module_demo_tag = get_module_demo_tag(module_tag, prog_lang_id)
        if module_demo_tag is not None:
            passed_count = module_demo_tag.get("passed_count")
            download_count = module_demo_tag.get("download_count")

        packets.append([nn,  # 0
                        prog_lang_tag.get("prog_lang_name"),  # 1
                        prog_lang_tag.get("prog_lang_alias"),  # 2
                        prog_lang_tag.get("prog_lang_type"),  # 3
                        prog_lang_tag.get("compiler_name"),  # 4
                        prog_lang_tag.get("platform"),  # 5
                        passed_count,  # 6
                        download_count  # 7
                        ])

    return packets


def get_nss_comma_list(nss_tag):
    nss_list = ""
    if nss_tag is None:
        return nss_list

    for ns_tag in nss_tag:
        n = ns_tag.get("n")
        if n == "delphi":
            continue

        if len(nss_list) > 0:
            nss_list += ", "

        nss_list += n
    return nss_list


def get_module_info(module_tag):
    module_info_tag = xml_search_tag(module_tag, 'info')
    nss_tag = xml_search_tag(module_tag, 'nss')
    module_type = module_tag.get('k')
    module_name = module_tag.get('id')
    if module_info_tag is None:
        module_info = dict(name=module_name, type=module_type,
                           namespace="",
                           required_nss="",
                           nss="",
                           project=None,
                           version="1",
                           owner="",
                           date="")
    else:
        module_info = dict(name=module_name, type=module_type,
                           namespace=module_info_tag.get('namespace'),
                           required_nss=module_info_tag.get('required'),
                           nss=get_nss_comma_list(nss_tag),
                           project=module_info_tag.get('project'),
                           version=module_tag.get('version'),
                           owner=module_info_tag.get('owner'),
                           date=module_info_tag.get('date'))

    author = module_info['owner']
    author_name = ""
    author_login = ""
    if author is not None:
        authors = author.split('||')
        if len(authors) > 1:
            author_name = authors[0]
            author_login = authors[1]
        else:
            author_name = author
            author_login = ""
    module_info.update(author_name=author_name, author_login=author_login)
    return module_info


def get_code_func_params(func_tag):
    func_params = []
    func_params_tag = xml_search_tag(func_tag, 'params')
    if func_params_tag is None:
        return func_params

    nn = 0
    for param_tag in func_params_tag:
        param_type = param_tag.get("t")
        for id_tag in param_tag:
            param_id = id_tag.get("n")
            nn += 1
            func_params.append([nn, param_type, param_id])

    return func_params


def get_func_rels_comma_list(rels_tag):
    if rels_tag is None:
        return ""
    rels_list = ""
    for tag in rels_tag:
        if len(rels_list) > 0:
            rels_list += ", "
        rels_list += tag.get('n')
    return rels_list


def get_module_funcs(module_name, module_funcs_tag):
    funcs = []

    nn = 0
    for func_tag in module_funcs_tag:
        if func_tag.tag != 'func':
            continue

        func_name = func_tag.get('id')
        func_vis = func_tag.get('b')
        func_mode = 'func'
        func_type = func_tag.get('t')
        if (func_type is None) or (len(func_type) == 0):
            func_type = "void"

        if func_vis != "pub":
            if func_name == '_main_':
                func_name = module_name
                func_mode = "project"
                func_type = ""
            else:
                continue

        func_rels_tag = xml_search_tag(func_tag, 'rels')
        func_params = get_code_func_params(func_tag)

        func_rels = get_func_rels_comma_list(func_rels_tag)

        nn += 1     # 0     1           2         3           4         5
        funcs.append([nn, func_name, func_type, func_params, func_mode, func_rels])
    return funcs


def get_module_classes(module_classes_tag):
    classes = []
    nn = 0
    for class_tag in module_classes_tag:
        if class_tag.tag != 'class':
            continue
        func_vis = class_tag.get('b')
        if func_vis != "pub":
            continue

        class_name = class_tag.get('n')
        nn += 1     # 0     1           2         3           4         5
        classes.append([nn, class_name, "", "", 'class', []])

    return classes


def get_module_records(module_types_tag):
    recs = []
    nn = 0
    for type_tag in module_types_tag:
        if type_tag.tag != 'type':
            continue

        x_tag = type_tag[0]
        k = x_tag.get('k')
        if k != 'record':
            continue

        type_name = type_tag.get('n')
        nn += 1     # 0     1       2    3   4     5
        recs.append([nn, type_name, "", "", 'rec', []])

    return recs


def get_module_cells(module_tag):
    cells = []

    module_name = module_tag.get('id')

    # funcs
    module_funcs_tag = xml_search_tag(module_tag, 'functions')
    if module_funcs_tag is not None:
        cells += get_module_funcs(module_name, module_funcs_tag)

    # classes
    module_classes_tag = xml_search_tag(module_tag, 'classes')
    if module_classes_tag is not None:
        cells += get_module_classes(module_classes_tag)

    # records
    module_types_tag = xml_search_tag(module_tag, 'types')
    if module_types_tag is not None:
        cells += get_module_records(module_types_tag)

    return cells


def get_ext_by_source_name(name):
    if name == 'pascal':
        return 'pas'
    if name == 'python':
        return 'py'
    if name == 'cpp':
        return 'cpp'
    return name


def get_alg_def(alg_tag, def_name, def_lang):
    if alg_tag is None:
        return "N/A"

    defs_tag = xml_search_tag(alg_tag, "defs")
    if defs_tag is None:
        return "N/A"

    for def_tag in defs_tag:
        if def_tag.get('def_name') == def_name:
            if def_tag.get('lang_ident') == def_lang:
                return def_tag.get('def_value')

    return "N/A"


def get_alg_def_notation_tag(alg_tag):
    alg_def_notation_tag = None
    alg_notations_tag = xml_search_tag(alg_tag, 'notations')
    if alg_notations_tag is not None:
        alg_def_notation_tag = xml_search_tag(alg_notations_tag, 'def_notation')
    return alg_def_notation_tag


def get_alg_rels(alg_notation_tag):
    rels_tag = xml_search_tag(alg_notation_tag, "rels")
    if rels_tag is None:
        return []

    rels = []
    nn = 0
    for rel_tag in rels_tag:
        info = rel_tag.get("info")
        if info is not None:
            continue

        rels.append([nn,  # 0
                     rel_tag.get("name"),  # 1
                     rel_tag.get("group"),  # 2
                     rel_tag.get("module_name"),  # 3
                     rel_tag.get("module_owner"),  # 4
                     rel_tag.get("name_id"),  # 5
                     rel_tag.get("module_id"),  # 6
                     rel_tag.get("ref_name"),  # 7
                     rel_tag.get("ref_group_name"),  # 8
                     rel_tag.get("rel_name"),  # 9
                     rel_tag.get("rel_group"),  # 10
                     rel_tag.get("demo_module"),  # 11
                     rel_tag.get("demo_owner"),  # 12
                     rel_tag.get("is_passed"),  # 13
                     rel_tag.get("is_tested")  # 14
                     ])
        nn += 1

    return rels


def get_alg_rel_by_module_id(alg_rels, module_id):
    for alg_rel in alg_rels:
        if alg_rel[6] == module_id:
            return alg_rel
    return None


def get_alg_rel_by_ref_names(alg_rels, ref_alg_names):
    if len(ref_alg_names) == 4:
        for alg_rel in alg_rels:
            if (alg_rel[2] == ref_alg_names[0]) and \
               (alg_rel[1] == ref_alg_names[1]) and \
               (alg_rel[4] == ref_alg_names[2]) and \
               (alg_rel[3] == ref_alg_names[3]):
                return alg_rel
    return None


def make_wsa_config(params):
    config = '<?xml version="1.0" ?>\r\n'
    config += '<project id = "wikinomy.%s" as="%s" >\r\n' % \
              (params["project_name"], params["project_alias"])

    names = params["names"]
    if len(names) > 0:
        config += '  <names>\r\n'
        for name in names:
            config += '    <resolve id="%s" as="%s"/>\r\n' % (name[0], name[1])
        config += '  </names>\r\n'

    config += '  <compile lang="%s" />\r\n' % get_safe_param(params, "compile")
    config += '</project>'
    return config


def make_wsa_pack_config(params):
    config = '<?xml version="1.0" ?>\r\n'
    config += '<project id = "%s" action="pack" >\r\n' % \
              (params["project_name"])

    modules = get_safe_param(params, "modules")
    if modules is not None:
        if len(modules) > 0:

            if get_safe_param(params, "fix_list", "") == "1":
                config += '  <pack fix="1">\r\n'
            else:
                config += '  <pack>\r\n'
            for module in modules:
                config += '  <module id="%s"/>\r\n' % (module[0])
            config += '  </pack>\r\n'

    config += '  <compile lang="%s" />\r\n' % get_safe_param(params, "compile")
    config += '</project>'
    return config


def safe_filename(data):
    return translit.slugify(data).lower()


def safe_check_cr_crlf(rows):
    if len(rows) == 1:   # <CR>   -> <CR><LF>
        # test()
        rows = rows[0].split('\r')
        i = 0
        for row in rows:
            rows[i] = row + '\n'
            i += 1
        return rows
    else:

        return rows


def make_text_from_rows(rows):
    t = ""
    for s in rows:
        t += s + " "
    return t


def get_descr_tags(tags_tag):
    cells = list()

    nn = 0
    for tag in tags_tag:
        if tag.tag == "tag":
            tag_name = tag.get("name")
            ru_tag = xml_search_tag(tag, "ru")
            if ru_tag is not None:
                nn += 1
                cells.append([str(nn), "tag", tag_name, "title", "ru", ru_tag.get("title")])
    return cells


def get_descr_alg(ns_nn, ns_name, tag_alg, nn):
    cells = list()

    alg_name = ns_name+'.'+tag_alg.get("name")
    for stag in tag_alg:
        if stag.tag == "params":
            for ptag in stag:
                param_name = ptag.get("name")
                for dtag in ptag:
                    s = dtag.get("title")
                    cells.append([str(ns_nn) + '.' + str(nn),
                                  "alg", alg_name, "param", param_name, dtag.tag, 'title', s])

        if stag.tag == "tags":
            s = stag.get("v")
            if s is not None:
                cells.append([str(ns_nn) + '.' + str(nn), "alg", alg_name, "tags", "en", s])

        if len(stag.tag) == 2:
            s = stag.get("title")
            if s is not None:
                cells.append([str(ns_nn) + '.' + str(nn), "alg", alg_name, "title", stag.tag, s])

            s = stag.get("descr")
            if s is not None:
                s = wiki_process(s)
                cells.append([str(ns_nn) + '.' + str(nn), "alg", alg_name, "descr", stag.tag, s])

            s = stag.get("note")
            if s is not None:
                cells.append([str(ns_nn) + '.' + str(nn), "alg", alg_name, "note", stag.tag, s])

            s = stag.get("kw")
            if s is not None:
                cells.append([str(ns_nn) + '.' + str(nn), "alg", alg_name, "kw", stag.tag, s])

    return cells


def get_descr_ns(tag_ns, nn):
    cells = list()

    ns_name = tag_ns.get("name")
    ru_tag = xml_search_tag(tag_ns, "ru")
    if ru_tag is None:
        cells.append([str(nn), "namespace", ns_name, "title", "en", ""])
    else:
        cells.append([str(nn), "namespace", ns_name, "title", "ru", ru_tag.get("title")])

    n_alg = 0
    for tag in tag_ns:
        if tag.tag == "alg":
            n_alg += 1
            cells.extend(get_descr_alg(nn, ns_name, tag, n_alg))

    return cells


def get_descr_cells(descr_tag):
    cells = list()

    nn_ns = 0
    for tag in descr_tag:
        if tag.tag == "tags":
            cells.extend(get_descr_tags(tag))
        if tag.tag == "ns":
            nn_ns += 1
            cells.extend(get_descr_ns(tag, nn_ns))
    return cells


def wiki_process(s):
    ss = s.replace('#br#', '<br/>')

    while True:
        p = ss.find('#a#')
        if p < 0:
            break

        sl = ss[0:p]
        ss = ss[p+3:]

        p = ss.find('#/a#')
        if p < 0:
            break

        sm = ss[0:p]
        sr = ss[p+4:]

        p = sm.find('#href#')
        sh = sm
        if p >= 0:
            sh = sm[p+6:]
            sm = sm[:p]

        ss = '%s<a href="%s" target="_blank">%s</a>%s' % (sl, sh, sm, sr)

    return ss


def extract_def_info_from_alg_tag(alg_tag):
    alg_name2 = alg_tag.get("alg_name")

    alg_title = None
    alg_note = None
    alg_kwords = None
    alg_descr = None

    defs_tag = xml_search_tag(alg_tag, 'defs')
    if defs_tag is None:
        return alg_title, alg_note, alg_kwords, alg_descr

    for def_tag in defs_tag:
        def_id = def_tag.get("def_id")
        if def_id is None:
            continue

        def_id = int(def_id)
        if def_id == 1:
            if alg_title is None:
                alg_title = def_tag.get("def_value")
        if def_id == 2:
            if alg_descr is None:
                alg_descr = def_tag.get("def_value")
        if def_id == 3:
            if alg_note is None:
                alg_note = def_tag.get("def_value")
        if def_id == 4:
            if alg_kwords is None:
                alg_kwords = def_tag.get("def_value")

    if alg_title is None:
        alg_title = from_upper(alg_name2)

    if alg_note is None:
        if alg_descr is None:
            alg_note = alg_title
        else:
            if len(alg_descr) > 120:
                alg_note = alg_descr[:119]+'...'
            else:
                alg_note = alg_descr

    if alg_descr is None:
        alg_descr = "--"

    return alg_title, alg_note, alg_kwords, alg_descr


def make_path_names(path):
    names = []
    dir_list = os.listdir(path)
    nn = 0
    for dir_rec in dir_list:
        nn += 1
        (fn2, fn_ext) = os.path.splitext(dir_rec)

        stat = os.stat(os.path.join(path, dir_rec))
        names.append([nn, dir_rec, fn_ext[1:],
                      str(stat.st_size),
                      datetime.fromtimestamp(stat.st_atime).strftime('%d-%b-%Y %H:%M')])

    return names


def make_fill_fn(path, fn):
    return os.path.join(path, fn)


def make_user_module_list(modules_tag):
    modules = []
    nn = 0
    for module_tag in modules_tag:
        info = module_tag.get("info")
        if info is not None:
            continue

        nn += 1
        modules.append([
            nn,
            module_tag.get("module_name"),
            module_tag.get("LM"),
            module_tag.get("create_date")
        ])

    return modules


def get_project_params(project_info, default_project_info):
    if default_project_info is None:
        default_project_info = ''

    if (project_info is None) or (len(project_info) == 0):
        project_info = default_project_info

    names = project_info.split('.')

    if len(names) > 1:
        return names[0], names[1]
    return "", names[0]



