#!/usr/bin/python
# -*- coding: utf-8 -*-

from pcode.axgs import *
from pcode.lib import *
import base64


name_types = dict(ns="1", module="2", tag="3", func="4", project="5",
                  lib="6", param="7", notation="8", alias="9", nickname="10",
                  datatype="11", const="12", var="13", rec="14", wclass="15")


def do_update_group_info(pid, group_name, group_name_ru):
    # update group rec
    tag_xml = call_axgs_wiki('ai_name', {'code': pid, 'name': group_name, 'type_id': name_types["ns"]})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    group_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(group_id) == 0:
        group_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(group_id) == 0:
        return None, "request did not return group id"

    # update group title en
    tag_xml = call_axgs_wiki('au_def',
                             {'code': pid, 'lang': 'en', 'name_id': group_id,
                              'def': 'title', 'def_value': from_upper(group_name)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    # update group def ru
    if len(group_name_ru) > 0:
        tag_xml = call_axgs_wiki('au_def',
                                 {'code': pid, 'lang': 'ru', 'name_id': group_id,
                                  'def': 'title', 'def_value': from_upper(group_name_ru)})
        info = axgs_get_error(tag_xml)
        if info is not None:
            return None, info

    return group_id, None


def do_update_nickname_info(pid, user_name):
    # insert user nickname
    tag_xml = call_axgs_wiki('ai_name', {'code': pid, 'name': user_name, 'type_id': name_types["nickname"]})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    user_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(user_id) == 0:
        user_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(user_id) == 0:
        return None, "request did not return user id (%s)" % user_name

    return user_id, None


def do_update_module_info(pid, module_ident, group_id, ref_name_id="null"):
    # insert module (#2)
    tag_xml = call_axgs_wiki('ai_name2', {'code': pid, 'name': module_ident, 'type_id': name_types["module"],
                                          'parent_name_id': group_id, 'ref_name_id': ref_name_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    module_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(module_id) == 0:
        module_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(module_id) == 0:
        return None, "request did not return module id"

    return module_id, None


def do_update_project_info(pid, project_name, group_id):
    # insert project (#5)
    tag_xml = call_axgs_wiki('ai_name2', {'code': pid, 'name': project_name, 'type_id': "5",
                                          'parent_name_id': group_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    project_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(project_id) == 0:
        project_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(project_id) == 0:
        return None, "request did not return project id (%s)" % project_name

    return project_id, None


def do_update_alg_notation(pid, notation, alg_id):
    # insert notation (#8)
    tag_xml = call_axgs_wiki('ai_name2', {'code': pid, 'name': notation, 'type_id': "8",
                                          'parent_name_id': alg_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    notation_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(notation_id) == 0:
        notation_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(notation_id) == 0:
        return None, "request did not return notation id"

    return notation_id, None


def do_update_alg_param_def_field(pid, alg_name, param_name, field_lang, field_name, field_value):
    algs = alg_name.split('.')

    tag_xml = call_axgs_wiki('q_alg_param',
                             {'code': pid, 'group_name': algs[0], 'alg_name': algs[1], 'param_name': param_name})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    param_id = xml_get_value_by_path(tag_xml, 'alg.alg_name.def_notation.param.param_id')
    if len(param_id) == 0:
        return None, "request did not return param id (%s.%s - %s)" % (algs[0], algs[1], param_name)

    if (field_name == "") or (field_value == ""):
        return param_id, None

    tag_xml = call_axgs_wiki('au_def',
                             {'code': pid, 'lang': field_lang, 'name_id': param_id,
                              'def': field_name, 'def_value': field_value})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return param_id, info

    return param_id, None


def do_update_alg_def_field(pid, alg_name, field_lang, field_name, field_value):
    algs = alg_name.split('.')

    type_id = name_types["ns"]
    tag_xml = call_axgs_wiki('ai_name',
                             {'code': pid, 'name': algs[0], 'type_id': type_id})

    group_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(group_id) == 0:
        group_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(group_id) == 0:
        return None, "request did not return group id (%s)" % algs[0]

    type_id = name_types["func"]
    tag_xml = call_axgs_wiki('ai_name2',
                             {'code': pid, 'name': algs[1], 'type_id': type_id,
                              'parent_name_id': group_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    name_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(name_id) == 0:
        name_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(name_id) == 0:
        return None, "request did not return alg id (%s.%s)" % (algs[0], algs[1])

    if (field_name == "") or (field_value == ""):
        return name_id, None

    tag_xml = call_axgs_wiki('au_def',
                             {'code': pid, 'lang': field_lang, 'name_id': name_id,
                              'def': field_name, 'def_value': field_value})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return name_id, 'cannot update def: '+info

    return name_id, None


def do_update_alg_info(pid, alg_name, alg_type, group_id, ref_name_id):
    # insert alg or project
    if alg_type == 'class':
        alg_type = 'wclass'

    type_id = name_types[alg_type]

    if ref_name_id is None:
        ref_name = "null"
    else:
        ref_name = ref_name_id

    tag_xml = call_axgs_wiki('ai_name2', {'code': pid, 'name': alg_name, 'type_id': type_id,
                                          'parent_name_id': group_id, 'ref_name_id': ref_name})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    alg_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(alg_id) == 0:
        alg_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(alg_id) == 0:
        return None, "request did not return alg id"

    return alg_id, None


def do_update_alg_ref(pid, name_id, alg_type):
    # insert alg type (#7)
    tag_xml = call_axgs_wiki('ai_name2', {'code': pid, 'name': alg_type, 'type_id': name_types["param"],
                                          'parent_name_id': name_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    ref_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(ref_id) == 0:
        ref_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(ref_id) == 0:
        return None, "request did not return ref id"

    tag_xml = call_axgs_wiki('au_name_ref', {'code': pid, 'name_id': name_id, 'ref_id': ref_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return ref_id, None


def do_update_link_name_module(pid, notation_id, module_id, id_order):
    # insert link notation-module
    tag_xml = call_axgs_wiki('ai_name_module',
                             {'code': pid, 'notation_id': notation_id, 'module_id': module_id,
                              'id_order': id_order})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return notation_id, None


def do_delete_link_name_module(pid, notation_id, module_id):
    # insert link notation-module
    tag_xml = call_axgs_wiki('ae_name_module',
                             {'code': pid, 'notation_id': notation_id, 'module_id': module_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return notation_id, None


def do_update_default_module(pid, notation_id, module_id):
    # insert default notation-module
    tag_xml = call_axgs_wiki('ai_default_module',
                             {'code': pid, 'notation_id': notation_id, 'module_id': module_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return notation_id, None


def do_delete_default_module(pid, notation_id, module_id):
    # clear link default notation-module
    tag_xml = call_axgs_wiki('ae_default_module',
                             {'code': pid, 'notation_id': notation_id, 'module_id': module_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return notation_id, None


def do_update_default_notation(pid, name_id, notation_id):
    # insert default alg-notation
    tag_xml = call_axgs_wiki('ai_default_notation',
                             {'code': pid, 'name_id': name_id, 'notation_id': notation_id})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return name_id, None


def do_update_link_name_demo(pid, notation_id, name_rel_id, module_rel_id):

    # insert link alg notation -> demo
    tag_xml = call_axgs_wiki('ai_name_demo',
                             {'code': pid, 'notation_id': notation_id,
                              'name_rel_id': name_rel_id,
                              'module_rel_id': module_rel_id})

    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return notation_id, None


def do_update_name_param(pid, notation_id, param_name, param_type, id_param_order):
    # 1. insert datatype (#11)
    tag_xml = call_axgs_wiki('ai_name', {'code': pid, 'type_id': name_types["datatype"], 'name': param_type})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, "insert tag name error: "+info

    type_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(type_id) == 0:
        type_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')

    # 2. insert param (#7)
    tag_xml = call_axgs_wiki('ai_name2', {'code': pid, 'name': param_name, 'type_id': name_types["param"],
                                          'parent_name_id': notation_id})

    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, "insert param name error: "+info

    param_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(param_id) == 0:
        param_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')
    if len(param_id) == 0:
        return None, "insert param name %s error: %s " % (param_name, "return empty value")

    # 3. insert link alg notation-param
    tag_xml = call_axgs_wiki('ai_name_param',
                             {'code': pid, 'notation_id': notation_id, 'param_id': param_id,
                              'type_id': type_id, 'id_order': str(id_param_order)})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, info

    return param_id, None


def do_update_name_tag(pid, name_id, tag_name, tag_title, tag_title_ru, id_tag_order):
    # insert tag (#3)
    tag_xml = call_axgs_wiki('ai_name', {'code': pid, 'type_id': name_types["tag"], 'name': tag_name})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, "insert tag name error|: "+info

    tag_id = xml_get_value_by_path(tag_xml, 'Auth.name.name_id')
    if len(tag_id) == 0:
        tag_id = xml_get_value_by_path(tag_xml, 'Auth.c_name.name_id')

    # insert tag def en
    tag_xml = call_axgs_wiki('au_def',
                             {'code': pid, 'lang': 'en', 'name_id': tag_id,
                              'def': 'title', 'def_value': tag_title})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return None, "update tag def error: " + info

    # insert tag def ru
    if len(tag_title_ru) > 0:
        tag_xml = call_axgs_wiki('au_def',
                                 {'code': pid, 'lang': 'ru', 'name_id': tag_id,
                                  'def': 'title', 'def_value': tag_title_ru})
        info = axgs_get_error(tag_xml)
        if info is not None:
            return None, "update tag def error: "+info

    # update name-tag link
    if len(name_id) > 0:
        id_tag_order += 1
        tag_xml = call_axgs_wiki('ai_name_tag',
                                 {'code': pid, 'tag_id': tag_id, 'name_id': name_id,
                                  'id_order': str(id_tag_order)})
        info = axgs_get_error(tag_xml)
        if info is not None:
            return None, "update name-tag link error: "+info

    return tag_id, None


def do_update_name_params(pid, notation_id, params):
    id_param_order = 0
    for param in params:
        param_type = param[1]
        param_name = param[2]
        id_param_order += 1
        (param_id, info) = do_update_name_param(pid, notation_id, param_name, param_type, id_param_order)
        if param_id is None:
            return info

    return None


def do_update_name_tags(pid, name_id, tags_str):
    if len(tags_str) == 0:
        return None

    tags = tags_str.split(',')
    id_tag_order = 0
    for tag in tags:
        tag_names = tag.split('||')
        tag_name = tag_names[0]
        tag_title = tag_name
        if len(tag_names) > 1:
            tag_title_ru = tag_names[1]
        else:
            tag_title_ru = ""

        id_tag_order += 1
        (tag_id, info) = do_update_name_tag(pid, name_id, tag_name, tag_title, tag_title_ru, id_tag_order)
        if tag_id is None:
            return info

    return None


def do_move_module_file_to_repository(pid, fn, owner_name, module_name):
    tag_xml = call_axgs_wiki('a_module_move',
                             {'code': pid, 'fn': fn, 'owner_name': owner_name, 'module_name': module_name})
    info = axgs_get_error(tag_xml)
    return info


def do_clear_name_relations(pid, module_id, notation_id):
    tag_xml = call_axgs_wiki('ae_name_rels',
                             {'code': pid, 'module_id': module_id, 'notation_id': notation_id})
    info = axgs_get_error(tag_xml)
    return info


def do_insert_name_relation(pid, module_id, notation_id, name_rel_id):
    tag_xml = call_axgs_wiki('ai_name_rel',
                             {'code': pid, 'module_id': module_id,
                              'notation_id': notation_id, 'name_rel_id': name_rel_id})
    info = axgs_get_error(tag_xml)
    return info


def do_publicate_name(pid, group_name, owner_name, module_name, module_func, project_id, wiki_names):
    func_name = module_func[1]
    func_type = module_func[2]
    func_params = module_func[3]
    func_mode = module_func[4]
    func_rels = module_func[5]

    # 1. update group info (#1)
    (group_id, info) = do_update_group_info(pid, group_name, "")
    if group_id is None:
        return None, "namespace update error: "+info

    # 2. update user info
    (user_id, info) = do_update_nickname_info(pid, owner_name)
    if user_id is None:
        return None, "user update error: "+info

    # 3. update module info (#2)
    (module_id, info) = do_update_module_info(pid, module_name, user_id)
    if module_id is None:
        return None, "module update error: "+info

    # 4. update alg info (#4)
    (alg_id, info) = do_update_alg_info(pid, func_name, func_mode, group_id, project_id)
    if alg_id is None:
        return "alg update error: " + info

    notation = get_alg_notation_signature(func_type, func_params)
    (notation_id, info) = do_update_alg_notation(pid, notation, alg_id)
    if notation_id is None:
        return alg_id, "alg notation update error: " + info

    # 5. update notation info
    (name_id, info) = do_update_default_notation(pid, alg_id, notation_id)
    if name_id is None:
        return alg_id, "alg default notation update error: " + info

    # 6. update link name-module info
    id_order = 0
    (name_id, info) = do_update_link_name_module(pid, notation_id, module_id, str(id_order))
    if name_id is None:
        return alg_id, "module link update error: " + info

    # 7. update default module rec
    (name_id, info) = do_update_default_module(pid, notation_id, module_id)
    if name_id is None:
        return alg_id, "notation default module update error: " + info

    # 8. update func params info
    if func_params is not None:
        if len(func_type) == 0:
            return alg_id, None

        (param_id, info) = do_update_name_param(pid, notation_id, "_self_", func_type, 0)
        if param_id is None:
            return alg_id, "alg ret param update error: " + info

        info = do_update_name_params(pid, notation_id, func_params)
        if info is not None:
            return alg_id, "alg params update error: " + info

    # 9. update relations info
    info = do_clear_name_relations(pid, module_id, notation_id)
    if info is not None:
        return alg_id, "alg rel clear error: " + info

    func_rels_list = func_rels.split(',')
    for rel_s in func_rels_list:
        rel = rel_s.strip()
        name_rel_id = get_name_rel_id(rel, wiki_names)
        if name_rel_id is None:
            continue

        info = do_insert_name_relation(pid, module_id, notation_id, name_rel_id)
        if info is not None:
            return alg_id, "alg rel insert error: " + info

    return alg_id, None


def do_cancel_publicate_name(pid, group_name, owner_name, module_name, module_func):
    func_name = module_func[1]
    func_type = module_func[2]
    func_params = module_func[3]

    # 1. update group info (#1)
    (group_id, info) = do_update_group_info(pid, group_name, "")
    if group_id is None:
        return None, "namespace update error: "+info

    # 2. update user info
    (user_id, info) = do_update_nickname_info(pid, owner_name)
    if user_id is None:
        return None, "user update error: "+info

    # 3. update module info (#2)
    (module_id, info) = do_update_module_info(pid, module_name, user_id)
    if module_id is None:
        return None, "module update error: "+info

    # 4. update alg info (#4)
    (alg_id, info) = do_update_alg_info(pid, func_name, 'func', group_id, None)
    if alg_id is None:
        return "alg update error: " + info

    notation = get_alg_notation_signature(func_type, func_params)
    (notation_id, info) = do_update_alg_notation(pid, notation, alg_id)
    if notation_id is None:
        return alg_id, "alg notation update error: " + info

    (name_id, info) = do_delete_link_name_module(pid, notation_id, module_id)
    if name_id is None:
        return alg_id, "module link delete error: " + info

    (name_id, info) = do_delete_default_module(pid, notation_id, module_id)
    if name_id is None:
        return alg_id, "notation default module delete error: " + info

    return alg_id, None


def do_test_name(code, test_tag, alg_names, ref_alg_names):
    compiler = test_tag.get('compiler')
    prog_lang_id = '1'
    if compiler == 'cpp':
        prog_lang_id = '2'

    group_name = ref_alg_names[0]
    alg_name = ref_alg_names[1]
    owner_name = ref_alg_names[2]
    module_name = ref_alg_names[3]

    tag_xml = call_axgs_wiki('q_alg',
                             {'alg_name': alg_name, 'group_name': group_name, 'project_group': 'test'})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return "->%s.%s.%s.%s %s test - error: " % (owner_name, module_name, group_name, alg_name, info)

    alg_tag = tag_xml[0]
    info = axgs_get_error(alg_tag)
    if info is not None:
        return "->%s.%s.%s.%s %s test - error: " % (owner_name, module_name, group_name, alg_name, info)

    alg_def_notation_tag = None
    alg_notations_tag = xml_search_tag(alg_tag, 'notations')
    if alg_notations_tag is not None:
        alg_def_notation_tag = xml_search_tag(alg_notations_tag, 'def_notation')

    if alg_def_notation_tag is None:
        return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, "def notation tag not found")

    alg_rels = get_alg_rels(alg_def_notation_tag)
    alg_rel = get_alg_rel_by_ref_names(alg_rels, alg_names)
    if alg_rel is None:
        return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, "alg_rel not found")

    rel_name = alg_rel[1]
    rel_group = alg_rel[2]

    rel_module = alg_rel[3]
    rel_owner = alg_rel[4]

    project_name = alg_rel[7]
    project_alias = owner_name+'_'+module_name+'_'+group_name+'_'+alg_name

    names = list()
    names.append(['wikinomy.'+group_name+'.'+alg_name, owner_name+'.'+module_name])
    names.append(['wikinomy.'+rel_group+'.'+rel_name, rel_owner+'.'+rel_module])

    wsa_config = make_wsa_config(dict(project_name=rel_group+'.'+project_name,
                                      project_alias=project_alias,
                                      names=names,
                                      compile=compiler))

    report = call_axgs_wiki_data('a_test_make',
                                 {'code': code, 'wsa_config': base64.b64encode(wsa_config),
                                  'project_alias': project_alias})
    if len(report) == 0:
        return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, 'a_test_make: report did not received')

    if report.count('?xml') > 0:
        tag_xml = axgs_xml_parse(report)
        info = axgs_get_error(tag_xml)
        if info is not None:
            return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, "make test code error: "+info)
        return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, report)

    if (report.count('error') > 0) or (report.count('Error') > 0) or (report.count('Fatal') > 0):
        return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, report)

    tag_xml = call_axgs_wiki('au_tested',
                             {'code': code,
                              'name': alg_name, 'group_name': group_name,
                              'module_name': module_name, 'owner_name': owner_name,
                              'rel_name': rel_name, 'rel_group': rel_group,
                              'rel_module_name': rel_module, 'rel_owner': rel_owner,
                              'prog_lang_id': prog_lang_id, 'is_tested': "1"})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return "->%s.%s.%s.%s test - error: %s" % \
                (owner_name, module_name, group_name, alg_name, info)

    return "->%s.%s.%s.%s %s test - passed OK" % (owner_name, module_name, group_name, alg_name, compiler)


def do_update_module_funcs(funcs, pid, group_id, module_id, project_id, wiki_names):
    id_order = 0
    for func in funcs:
        func_name = func[1]
        func_type = func[2]
        func_params = func[3]
        func_mode = func[4]
        func_rels = func[5]

        (alg_id, info) = do_update_alg_info(pid, func_name, func_mode, group_id, project_id)
        if alg_id is None:
            return "alg update error: "+info

        notation = get_alg_notation_signature(func_type, func_params)
        (notation_id, info) = do_update_alg_notation(pid, notation, alg_id)
        if notation_id is None:
            return "alg notation update error: "+info

        (name_id, info) = do_update_default_notation(pid, alg_id, notation_id)
        if name_id is None:
            return "alg default notation update error: "+info

        id_order += 1
        (name_id, info) = do_update_link_name_module(pid, notation_id, module_id, str(id_order))
        if name_id is None:
            return "module link update error: "+info

        (name_id, info) = do_update_default_module(pid, notation_id, module_id)
        if name_id is None:
            return "notation default module update error: "+info

        if func_params is not None:
            if len(func_type) == 0:
                continue

            (param_id, info) = do_update_name_param(pid, notation_id, "_self_", func_type, 0)
            if param_id is None:
                return "alg ret param update error: " + info

            info = do_update_name_params(pid, notation_id, func_params)
            if info is not None:
                return "alg params update error: " + info

        info = do_clear_name_relations(pid, module_id, notation_id)
        if info is not None:
            return "alg rel clear error: " + info

        func_rels_list = func_rels.split(',')
        for rel_s in func_rels_list:
            rel = rel_s.strip()
            name_rel_id = get_name_rel_id(rel, wiki_names)
            if name_rel_id is None:
                continue

            info = do_insert_name_relation(pid, module_id, notation_id, name_rel_id)
            if info is not None:
                return "alg rel insert error: " + info

    return None


def do_get_wiki_ns_list(ns):

    tag_xml = call_axgs_wiki('q_names', {'group_name': ns})
    info = axgs_get_error(tag_xml)
    if info is not None:
        return []

    n_list = []

    for tag_name in tag_xml:
        n_list.append([tag_name.get("name"), tag_name.get("name_id"), ns])

    return n_list


def get_wiki_names(nss):
    names = []

    nss_list = nss.split(',')
    for nss_item in nss_list:
        ns = nss_item.strip()
        if len(ns) == 0:
            continue
        n_list = do_get_wiki_ns_list(ns)
        names += n_list

    return names


def in_wiki_names(rel_name, names):
    for name in names:
        if rel_name == name[0]:
            return True

    return False


def search_wiki_name(names, rel_name):
    for name in names:
        if rel_name == name[0]:
            return name
    return None


def get_name_rel_id(rel_name, names):
    for name in names:
        if rel_name == name[0]:
            return name[1]

    return None


def get_func_wiki(func_rels, names):
    rels_list = func_rels.split(',')
    func_wiki = ""
    for rel_s in rels_list:
        rel_func = rel_s.strip()
        wiki_name = search_wiki_name(names, rel_func)

        if wiki_name is not None:
            if len(func_wiki) > 0:
                func_wiki += ", "
            func_wiki += wiki_name[2]+'.'+wiki_name[0]

        # if in_wiki_names(rel, names):
    return func_wiki


def do_update_funcs(funcs, names):
    for func in funcs:
        func_rels = func[5]
        func_wiki = get_func_wiki(func_rels, names)
        func.append(func_wiki)  # 6

    return funcs
