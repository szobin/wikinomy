#!/usr/bin/python
# -*- coding: utf-8 -*-
import zipfile
from pcode.db_lib import *
import base64


def zip_date_str(dt):
    return str(dt[2])+'.'+str(dt[1])+'.'+str(dt[0])


def get_zip_descr_list(zip_info_list):
    descr_list = []
    for zip_info in zip_info_list:
        fn = zip_info.filename
        if fn.count(".descr") > 0:
            descr_list.append(fn)
    return descr_list


def is_in_zip(zip_archive, fn):
    zip_info_list = zip_archive.infolist()
    for zip_info in zip_info_list:
        zip_fn = zip_info.filename
        if zip_fn == fn:
            return True
    return False


def module_zip_names(zip_fn):
    if not zipfile.is_zipfile(zip_fn):
        return []
    z = zipfile.ZipFile(zip_fn, 'r')
    info_list = z.infolist()
    zip_list = []
    nn = 0
    for zip_info in info_list:
        nn += 1
        zip_list.append([nn, zip_info.filename, zip_info.file_size, zip_date_str(zip_info.date_time)])
    z.close()
    return zip_list


def module_proc_xml_file(code, fn, file_content):
    xml_text = file_content.replace('\r', ' ')
    xml_text = xml_text.replace('\n', '')
    xml_tag = axgs_xml_parse(xml_text)
    if xml_tag.tag != "wiki":
        return fn + ' - error: wrong wiki descriptor format'

    # Insert desc file
    cells = get_descr_cells(xml_tag)
    for cell in cells:
        s_type = cell[1]

        if s_type == "namespace":
            (name_id, info) = do_update_group_info(code, cell[2], cell[5])
            if info is not None:
                return fn + " - error: update group info:  "+info

        if s_type == "tag":
            (name_id, info) = do_update_name_tag(code, "", cell[2], cell[2], cell[5], 0)
            if info is not None:
                return fn + " - error: update name tag: "+info

        if s_type == "alg":
            field_name = cell[3]
            if field_name == "param":
                (param_id, info) = do_update_alg_param_def_field(code, cell[2], cell[4], cell[5], cell[6], cell[7])
                if info is not None:
                    return fn + " - error: update alg param def field: "+info

            if field_name == "tags":
                (name_id, info) = do_update_alg_def_field(code, cell[2], "", "", "")
                if info is not None:
                    return fn + " - error: update alg def field: "+info

                info = do_update_name_tags(code, name_id, cell[5])
                if info is not None:
                    return fn + " - error: update name tags: "+info
            else:
                (name_id, info) = do_update_alg_def_field(code, cell[2], cell[4], field_name, cell[5])
                if info is not None:
                    return fn + " - error: update alg def field: "+info

    return fn + " - OK"


def module_proc_pas_file(code, user_name, module_name, fn, file_content, is_project=False):
    par_alg_content = base64.b64encode(file_content)
    par_alg_fn = fn

    # transfer and convert uploaded data
    report = call_axgs_wiki_data('a_module_upload',
                                 {'code': code, 'alg_code_content': par_alg_content, 'alg_code_fn': par_alg_fn})
    if len(report) == 0:
        return fn + ' - error: conversion: internal error'

    if report.count('?xml') > 0:
        tag_xml = axgs_xml_parse(report)
        info = axgs_get_error(tag_xml)
        if info is not None:
            return fn + ' - error: '+info
        return fn + ' - error: '+report

    if report.lower().count("error") > 0:
        return fn + ' - error: '+report

    (user_id, info) = do_update_nickname_info(code, user_name)
    if user_id is None:
        return fn + ' - error: user update error: '+info

    info = do_move_module_file_to_repository(code, module_name, user_name, module_name)
    if info is not None:
        return fn + ' - error: move module to repository error: '+info

    (module_id, info) = do_update_module_info(code, module_name, user_id)
    if module_id is None:
        return fn + ' - error: module update error: '+info

    return fn + ' -  OK'


def module_proc_obj_file(code, fn, file_content):
    return fn + ' -  passed'


def module_pass_file(fn):
    return fn + ' -  passed'


def module_proc_alg_tag(code, user_name, alg_tag, module_name, module_namespace, module_names, module_funcs, project_id, wiki_names):
    report = []
    alg_name_ex = alg_tag.get("n")
    if alg_name_ex is None:
        alg_name_ex = ''
    algs = alg_name_ex.split('.')

    if len(algs) == 1:
        group_name = module_namespace
        alg_name = algs[0]
    else:
        group_name = algs[0]
        alg_name = algs[1]

    alg_ref_name_ex = alg_tag.get("ref")
    if alg_ref_name_ex is None:
        alg_ref_name_ex = ''
    ref_alg_names = alg_ref_name_ex.split('.')

    module_func = module_get_name(module_funcs, alg_name)
    if module_func is None:
        report.append("*%s.%s publication error: alg not found in code of module %s" % (group_name, alg_name, module_name))
        return report

    #  в любом случае
    #  if module_get_name(module_names, alg_name) is None:
    (name_id, info) = do_publicate_name(code, group_name, user_name, module_name, module_func, project_id, wiki_names)
    if info is not None:
        report.append("*%s.%s publication error: %s" % (group_name, alg_name, info))
        return report

    report.append("*%s.%s - published OK" % (group_name, alg_name))

    alg_names = [group_name, module_func[1], user_name, module_name]
    for test_tag in alg_tag:
        report.append(do_test_name(code, test_tag, alg_names, ref_alg_names))

    return report


def module_proc_module_tag(code, user_name, module_tag, zip_archive):
    report = []

    #  1. catch info from module_tag
    module_filename = module_tag.get("n")
    module_namespace = module_tag.get("namespace")
    if module_namespace is None:
        module_namespace = ''

    module_nss = module_tag.get("required")
    if module_nss is None:
        module_nss = ''

    module_project = module_tag.get("project")
    if module_project is None:
        module_project = ''

    #  2. extract module file from zip
    if is_in_zip(zip_archive, module_filename) == 0:
        report.append(module_filename+': error: not found in zip archive')
        return report

    zip_ext_file = zip_archive.open(module_filename)
    module_content = zip_ext_file.read()

    #  3. move module file to wiki lib
    report.append(module_proc_file(code, user_name, module_filename, module_content))

    #  4. ask module code from wiki lib
    (module_name, module_file_ext) = os.path.splitext(module_filename)
    code_xml = call_axgs_wiki('q_module_xml', {'module_name': module_name, 'owner_name': user_name})
    info = axgs_get_error(code_xml)
    if info is not None:
        report.append("*%s.%s rq module code error: %s" % (user_name, module_name, info))
        return report

    #  5. ask module info params
    module_info = get_module_info(code_xml)
    module_funcs = get_module_cells(code_xml)

    group_name = module_info["namespace"]
    if group_name is None:
        group_name = module_namespace

    rq_nss = module_info['required_nss']
    if rq_nss is None:
        rq_nss = ''
    if rq_nss == '':
        rq_nss = module_nss

    #  6. get wiki names by required ns list
    wiki_names = get_wiki_names(rq_nss)

    #  7. get module elements list from lib
    tag_xml = call_axgs_wiki('q_module', {'module': module_name, 'owner': user_name})
    info = axgs_get_error(tag_xml, 3)
    if info is not None:
        report.append("*%s.%s module rq error: %s" % (user_name, module_name, info))
        return report

    lib_module_tag = tag_xml[0]

    module_name = lib_module_tag.get("module_name")
    module_names = get_module_names(lib_module_tag)

    # 8. project info
    (project_group, project_name) = get_project_params(module_project, module_info['project'])
    if project_group == '':
        project_group = group_name

    project_id = None
    if (project_name is not None) and (len(project_name) > 0):
        # 8.1 update project group info
        (group_id, info) = do_update_group_info(code, project_group, "")
        if group_id is None:
            report.append("*%s.%s module update project namespace %s error: %s" % (user_name, module_name, project_group, info))
            return report

        (project_id, info) = do_update_project_info(code, project_name, group_id)
        if project_id is None:
            report.append("*%s.%s module update project ref error: %s" % (user_name, module_name, info))
            return report

    for alg_tag in module_tag:
        report.extend(module_proc_alg_tag(code, user_name, alg_tag, module_name, group_name, module_names, module_funcs, project_id, wiki_names))

    return report


def module_proc_descr(code, user_name, descr_fn, zip_archive):
    report = []
    zip_ext_file = zip_archive.open(descr_fn)
    descr_content = zip_ext_file.read()

    xml_text = descr_content.replace('\r', ' ')
    xml_text = xml_text.replace('\n', '')
    xml_tag = axgs_xml_parse(xml_text)
    if xml_tag.tag != "descr":
        s = ""
        if xml_tag.tag == "axgs":
            s = xml_tag.get('info')
        report.append(descr_fn + ' - error: wrong module descriptor format '+s)
        return report

    for module_tag in xml_tag:
        report.extend(module_proc_module_tag(code, user_name, module_tag, zip_archive))

    report.append(descr_fn + ' - processed OK')
    return report


def module_proc_file(code, user_name, fn, file_content):
    (module_name, file_ext) = os.path.splitext(fn)
    file_ext = file_ext.lower()

    if file_ext == '.xml':
        return module_proc_xml_file(code, fn, file_content)

    if file_ext == '.pas':
        return module_proc_pas_file(code, user_name, module_name, fn, file_content)

    if file_ext == '.dpr':
        return module_proc_pas_file(code, user_name, module_name, fn, file_content, True)

    if file_ext == '.obj':
        return module_proc_obj_file(code, fn, file_content)

    if file_ext == '.dsk':
        return module_pass_file(fn)

    return fn + ' - error: unknown ext: '+file_ext


def module_report_errors(report):
    n = 0
    for s in report:
        n += s.count('error')
    return n


def module_proc_zip_file(code, user_name, zip_fn):
    if not zipfile.is_zipfile(zip_fn):
        return zip_fn + " - error: wrong zip format"
    z = zipfile.ZipFile(zip_fn, 'r')
    info_list = z.infolist()
    report = []
    descr_list = get_zip_descr_list(info_list)
    if len(descr_list) > 0:
        for descr_name in descr_list:
            report.extend(module_proc_descr(code, user_name, descr_name, z))
        z.close()
        return report

    for zip_info in info_list:
        if zip_info.file_size == 0:
            continue
        zip_ext_file = z.open(zip_info.filename)
        report.append(module_proc_file(code, user_name, zip_info.filename, zip_ext_file.read()))

    z.close()
    return report


def module_get_name(module_names, name):
    for module_name in module_names:
        if module_name[1] == name:
            return module_name

    return None


def module_join_names_and_funcs(module_funcs, module_names):
    names = []
    nn = 0
    for func in module_funcs:
        nn += 1
        cell_name = func[1]
        cell_type = func[4]
        name = [nn, cell_name, cell_type]
        module_name = module_get_name(module_names, cell_name)
        if module_name is None:
            name.append('local')
            name.append('1')
        else:
            name.append(module_name[2])
            name.append('')

        names.append(name)

    return names
